<?php
// mango
// Update

defined('BASEPATH') OR exit('No direct script access allowed');

class Join extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['page'] = 'crop';
		$this->load->view('template/template', $data);
	}

	public function test_hover()
	{
		$data['page'] = 'hover';
		$this->load->view('template/template', $data);
	}

	public function crop()
	{
		$data['page'] = 'crop';
		$this->load->view('template/template', $data);
		//$this->load->view('crop2');
	}

	public function form($error_response = "")
	{
		//print_r($this->input->post('comp_image'));
		//echo date('YmdHis');
		//echo $six_digit_random_number = md5(mt_rand(111111, 999999))."-".date('YmdHis');
		
		$data['raw_image_data'] = $this->input->post('comp_image');
		if (isset($data['raw_image_data']) || $data['raw_image_data']) {
			$data['error_response'] = $error_response;
			//echo $uri_string = urlencode($this->encrypt->encode('ok'));
			$data['page'] = 'form';
			$this->load->view('template/template', $data);
		}else{
			$page = '';
			show_404($page, $log_error = TRUE);
		}
	}

	public function validate()
	{

		//print_r($this->input->post());
		$this->form_validation->set_rules('name', 'name', 'trim|required');
		$this->form_validation->set_rules('contact', 'contact number', 'trim|required');
		$this->form_validation->set_rules('email', 'e-mail', 'trim|required|valid_email');
		$this->form_validation->set_rules('address', 'address', 'trim|required');
		$this->form_validation->set_rules('birthdate', 'birthdate', 'trim|required');
		$this->form_validation->set_rules('raw_image_data', 'raw_image_data', 'trim|required');

		//IF Validation returns FALSE reload index and display error message
		//ELSE load sign-up model and call signUp function
		$raw_data = $this->input->post('raw_image_data');
		if ($this->form_validation->run() == FALSE)
		{
			validation_errors();
			if (isset($raw_data) || $raw_data == "") {
				$error_response = "Your session has expired, please upload your photo again!";
			}else{
				$error_response = "Please check your information";
			}
			
			$this->form($error_response);

		}else{
			
			//Write file first then save to DB
			$re = $this->write_image_from_raw($raw_data);

			if ($re['status']) {
				
				$this->load->model('registration_model');
				$return 	= $this->registration_model->sign_up($re['file_name']);

	 			$uri_string = urlencode($this->encrypt->encode('ok'));
	 			$user_id 	= urlencode($this->encrypt->encode($return['user_id']));
	 			$entry_id	= $return[last_id];
				if ($return['status']) {
					redirect(base_url().'join/thank-you?i='.$uri_string.'&m='.$re['file_name'].'&eid='.$entry_id.'&uid='.$user_id);
				}else{
					show_error('message',500);
					// $error_response = "";
					// $this->form($error_response);				
				}

			}else{
				//echo "Unable to save image!";
				show_error('message',500);				
			}

			//echo "CORRECT";
			// $this->load->model('fmodel_events');
			// $response = $this->fmodel_events->sign_up();

			// # Redirect to form page again with response message
			// // $this->session->set_flashdata('status_err_msg', $response['message']);
			// // redirect(base_url().'events/');

			// if ($response['status']) {
			// 	$this->session->set_flashdata('status_err_msg', $response['message']);
			// 	redirect(base_url().'events/');
			// }

		}		
	}

	public function write_image_from_raw($raw_data = "", $path = "entries/"){

		$return['status'] 	 = "";
		$return['file_name'] = "";
		if ($raw_data) {
			$replace_raw   = str_replace("[removed]", "", $raw_data);
			$stripped_data = base64_decode($replace_raw);
			//$stripped_data = "data:image/png;base64," . $replace_raw;
			$file_name     = md5(mt_rand(111111, 999999))."-".date('YmdHis').".jpg";
			//$path 		   = "entries/";
			// $fp = fopen('.entries','wb+');
			// fwrite($fp,$image.".jpg");
			// fclose($fp);		
			//echo $image_data = file_get_contents($_REQUEST['url']);
			//file_put_contents('entries/image-1-1.jpg', $foo);

			if ( ! write_file($path.$file_name, $stripped_data))
			{
				$return['status'] 	 = FALSE;
				$return['file_name'] = "";

			}else{
				$return['status'] 	 = TRUE;
				$return['file_name'] = $file_name;

			}		
		}else{
			$page = '';
			show_404($page, $log_error = TRUE);
			
		}

		return $return;
	}
	
	public function save()
	{
		if($this->input->post('comp_image'))
		{
			$return = $this->write_image_from_raw($this->input->post('comp_image'), 'saved_entries/');

			if($return['status'] == TRUE)
			{
				$data['filename'] = $return['file_name'];

				$data['page'] = 'save';
				//$data['filename'] = "data:image/png;base64," . str_replace("[removed]", "", $this->input->post('comp_image'));

				$this->load->view('template/template', $data);
			}else
			{
				redirect('join');
			}
		}else{
			 redirect('join');
		}
	}

	public function thank_you()
	{
		$page = '';
		$uid  = '';
		$uri  = $this->input->get('i');
		$data['file_name'] = "img-user-placeholder.jpg";
		$data['details']   = "";


		$this->encrypt->decode($uri);
		if(isset($uri)){
			if ($this->encrypt->decode($uri) == 'ok') {

				if ($this->input->get('m')) {
					$data['file_name'] = $this->input->get('m');
					$uid = $this->encrypt->decode($this->input->get('uid'));
				}
				$eid = $this->input->get('eid');

 				$this->load->model('registration_model');
 				$return 	= $this->registration_model->fetch_account_details($uid);				
 				
				$data['details'] = $return['account_details'][0];
				$data['entry_id'] = $eid;
				$data['page'] = 'thank-you';
				$this->load->view('template/template', $data);

			}else{
				 show_404($page, $log_error = TRUE);
			}
		}else{
			 show_404($page, $log_error = TRUE);
		}
	}
}
