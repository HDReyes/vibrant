<?php
// Update

defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['page'] = 'gallery';
		$this->load->view('template/template', $data);
	}


	public function fetch_gallery(){
		header('Content-Type: application/json');

		if ($this->input->post('ajax') == 1) {
			$from = $this->security->xss_clean($this->input->post('from'));

			$this->load->model('gallery_model');
			$return = $this->gallery_model->gallery_data($from);
			echo json_encode($return);

		}
	}

	public function r()
	{
		$id = (int) $this->uri->segment(3);

		if($id > 0)
		{
			$this->load->model('gallery_model');
			$this->load->library('user_agent');

			$info = $this->gallery_model->fetch_entry_details($id);

			if ($this->input->server('HTTP_REFERER')) 
			{
				redirect('gallery/index');
			}
			elseif ($this->input->server('SERVER_NAME') && !strpos($this->input->server('HTTP_USER_AGENT'),"facebook.com/externalhit_uatext")) // means copy pasted, but not fb sharer.php
			{
				redirect('gallery/index');		
			}
			else
			{
				$this->load->view('referer.php', $info);
			} 

			//redirect('gallery/index');
		}
	}

	public function page()
	{
		if ($this->input->get('u')) {

			$this->load->model('gallery_model');
			$data = $this->gallery_model->fetch_entry_details($this->input->get('u'));
			$data['page'] = 'profile';
			$this->load->view('template/template', $data);

		}else{
			show_404($page, $log_error = TRUE);
		}
	}
}
