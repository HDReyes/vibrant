<?php
// Update

defined('BASEPATH') OR exit('No direct script access allowed');

class Videos extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['page'] = 'videos';
		$this->load->view('template/template', $data);
	}


	public function fetch_video()
	{
		header('Content-Type: application/json');
	 	$return['status'] 		= "";
	 	$return['video_url'] 	= "";

		if ($this->input->post('ajax') == 1) {
			
			$vid = $this->input->post('vid_id');
		 	$return['status'] = TRUE;

			switch ($vid) {
				case '1':
					$return['video_url'] = "https://www.youtube.com/embed/XIjOlLUA6SQ";
					break;
				case '2':
					$return['video_url'] = "https://www.youtube.com/embed/PHTIWT7cDUM";
					break;
				case '3':
					$return['video_url'] = "https://www.youtube.com/embed/wGbS0aTzwY4";
					break;
				case '4':
					$return['video_url'] = "https://www.youtube.com/embed/YWweEE_vwdg";
					break;
				case '5':
					$return['video_url'] = "https://www.youtube.com/embed/teosq3IyZ3c";
					break;
				case '6':
					$return['video_url'] = "https://www.youtube.com/embed/IvZbUJySkbk";
					break;
				
				default:
					$return['video_url'] = "https://www.youtube.com/embed/XIjOlLUA6SQ";
					break;
			}


		}else{
			$return['status'] = FALSE;
		}
		echo json_encode($return);

	}



// EASY BRAIDED BUN
// https://www.youtube.com/watch?v=XIjOlLUA6SQ&index=3&list=PLtuoSpLkjtPcPhMvdRmql81AZQlaxyK-V
 
// CRISSCROSS PONYTAIL
// https://www.youtube.com/watch?v=PHTIWT7cDUM
 
// HALF CROWN BRAID
// https://www.youtube.com/watch?v=wGbS0aTzwY4
 
// Silky Straight
// https://www.youtube.com/watch?v=YWweEE_vwdg&list=PLtuoSpLkjtPcPhMvdRmql81AZQlaxyK-V&index=5
 
// Bangong Palmolive
// https://www.youtube.com/watch?v=teosq3IyZ3c&index=8&list=PLtuoSpLkjtPcPhMvdRmql81AZQlaxyK-V
 
// Anti-Hair Fall
// https://www.youtube.com/watch?v=IvZbUJySkbk&list=PLtuoSpLkjtPcPhMvdRmql81AZQlaxyK-V&index=9

}
