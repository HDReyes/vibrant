<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Update

class Registration_model extends CI_Model {

	 public function sign_up($img_name = "")
	 {
		$return['status']  = "";
		$return['message'] = "";
		$return['user_id'] = "";
		$return['last_id'] = "";

		$name  		 = ucwords($this->input->post('name', TRUE));
		$contactno   = $this->input->post('contact', TRUE);
		$email  	 = $this->input->post('email', TRUE);
		$address  	 = $this->input->post('address', TRUE);
		$birthdate   = $this->input->post('birthdate', TRUE);
		$file_name	 = $img_name;

		// Check if account exist
		$account_details = $this->check_account_details($email);

		//print_r($account_details['status']);
		if ($account_details['status']) {
			
			$user_id = $account_details['account_details'][0]['user_id'];	
			$return = $this->insert_into_entries($file_name, $user_id);
			
			if ($return['status']) {
					$return['message'] = "YEHEY!";
			}

			//We'll return the user id to query details on thank you page
			$return['user_id'] = $user_id;
			$return['last_id'] = $return['last_id'];

		}else{

			// Inserts new entry & account when NO Account is detected
			$values = 'name 		=	"' . $this->security->xss_clean($name) . '", '.
					  'email 		=	"' . $this->security->xss_clean($email) . '", '.
					  'mobile 	 	=	"' . $this->security->xss_clean($contactno) . '", '.
					  'address 	 	=	"' . $this->security->xss_clean($address) . '", '.
					  'status 	 	=	"' . $this->security->xss_clean('1') . '", '.
					  'entry_date	=	"' . date('Y-m-d H:i:s') . '"';

			$query = 'INSERT INTO tbl_users SET ' . $values;
			$insert = $this->db->query($query);
			if($this->db->affected_rows() > 0)
			{
				$user_id = $this->db->insert_id();
				$return  = $this->insert_into_entries($file_name, $user_id);
				
				if ($return['status']) {
					$return['message'] = "YEHEY!";
				}

				//We'll return the user id to query details on thank you page
				$return['user_id'] = $user_id;
				$return['last_id'] = $return['last_id'];

			}else{
				$return['status']  = FALSE;
				$return['message'] = "Oops, something went wrong. Try sending again, Thank you!";
			}

		}
		

		return $return;
	 }

	 // Checks if account/E-mail exist
	 public function check_account_details($email = "")
	 {
		 	$return['status'] 			= "";
		 	$return['account_details'] 	= "";
			$sql = "SELECT * FROM tbl_users WHERE email = ?";
			$query = $this->db->query($sql, $email);
			if($query->num_rows() > 0)
			{
			 	$return['status'] 			= TRUE;
			 	$return['account_details'] 	= $query->result_array();

			}else{
			 	$return['status'] 			= FALSE;
			}

			return $return;
	 }

	// Inserts new entry
	 public function insert_into_entries($image_name = "", $user_id = "")
	 {
	 		$return['status'] = FALSE;
	 		$return['last_id'] = "";

	 		$values = 'user_id 			=	"' . $this->security->xss_clean($user_id) . '", '.
					  'image_name 		=	"' . $this->security->xss_clean($image_name) . '", '.
					  'entry_status 	=	"' . $this->security->xss_clean('0') . '", '.
					  'date_submitted	=	"' . date('Y-m-d H:i:s') . '"';

			$query = 'INSERT INTO tbl_entry SET ' . $values;
			$insert = $this->db->query($query);
			if($this->db->affected_rows() > 0)
			{
				$return['last_id'] = $this->db->insert_id();
				$return['status']  =  TRUE;

			}
			return $return;
	 }

	 // Fetch account details
	 public function fetch_account_details($user_id = "")
	 {
		 	$return['status'] 			= "";
		 	$return['account_details'] 	= "";
			$sql = "SELECT * FROM tbl_users WHERE user_id = ?";
			$query = $this->db->query($sql, $user_id);
			if($query->num_rows() > 0)
			{
			 	$return['status'] 			= TRUE;
			 	$return['account_details'] 	= $query->result_array();

			}else{
			 	$return['status'] 			= FALSE;
			}

			return $return;
	 }


}
