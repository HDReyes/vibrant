<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Update

class Gallery_model extends CI_Model {

	 public function gallery_data($from = "")
	 {
 		$range = 0;
 		if ($from > 0) {
 			$range = $from;
 		}
	 	$return['status'] 			= "";
	 	$return['gallery_data'] 	= "";
		$sql = "SELECT users.name, users.user_id, entry.image_name, entry.date_submitted, entry.entry_id, entry.entry_status FROM tbl_entry as entry, tbl_users as users WHERE users.user_id = entry.user_id AND entry.entry_status = '1' ORDER BY entry.date_submitted DESC, RAND() LIMIT ".$range.", 9";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
		 	$return['status'] 			= TRUE;
		 	$return['gallery_data'] 	= $query->result();

		}else{
		 	$return['status'] 			= FALSE;
		}

		return $return;
	 }

	 // Fetch account details
	 public function fetch_entry_details($entry_id = "")
	 {
		 	$return['status'] 			= "";
		 	$return['account_details'] 	= "";
			$sql = "SELECT a.*, b.* FROM tbl_entry as a, tbl_users as b WHERE a.user_id = b.user_id AND a.entry_id = ?";
			$query = $this->db->query($sql, $entry_id);
			if($query->num_rows() > 0)
			{
			 	$return['status'] 			= TRUE;
			 	$return['account_details'] 	= $query->result_array();

			}else{
			 	$return['status'] 			= FALSE;
			}

			return $return;
	 }

}
