    <!--Main Content-->
    <div class="container-fluid main-content thank-you">
        <?php //if(!isset($_COOKIE["removeHowTo"])){ ?>
           <!--Guide-->
            <div class="row col-xs-12 col-sm-12 pull-left">
                <div id="howTo" class="col-xs-12 col-sm-8 col-sm-offset-4 col-md-6 col-md-offset-6 alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close" id="closeHowTo">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="body">
                        <div class="col-xs-12 col-sm-12">
<!--                             <br>
                            <h1 style="text-align:center !important">Thank You!</h1>
                            <br>
 -->                            
                            <h5 style="text-align:center !important; font-size:19px !important">Check out the <a href="<?php echo base_url(); ?>gallery/" style="font: Normal 40px/50px 'Manhattan Darling', Arial, Sans-serif; color:#d7113e">gallery</a> to see your valid entry.</h5>
                            <br>
                            <div class="row">
                                <div class="col-xs-12">

                                    <p style="font-size:14px !important; text-align:center">Want to save this photo? Just click on the save button below, right-click the image, then 'Save As'.</p>
                                    <br>
                                    <p style="font-size:14px !important; text-align:center">Don't forget to share your photo using the hashtag #PalmoliveStayVibrant</p>

                                </div>
                            </div>                            

<!--                             <div class="col-xs-12">
                                <h5 style="text-align:center !important; font-size:18px !important">Share with the #PalmoliveStayVibrant</h5> 
                            </div>
 -->                            
                        </div>
                    </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12" style="text-align:center; font-weight:normal; font-family:arial; font-size:11px; font-style:italic"><br><br>It might take a while for the entry to show up. <br>All photos are subjected to screening for the validity of entry.</div>
                        </div>
                </div>
            </div>
            <!--End of Guide-->
        <?php //} ?>    

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-1 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                    <h1>Thank You!</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-1 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 text-center userImage2 effect">
                    <img src="<?php echo base_url(); ?>entries/<?php echo $file_name; ?>" alt="">
                    <strong class="col-md-12 userName"><?php echo ucwords($details['name']); ?></strong>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4 col-md-offset-1 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 text-center">
                    <button type="button" onclick="window.open('<?php echo base_url(); ?>entries/<?php echo $file_name; ?>', '_blank');" class="btn generic upload-config-btns save-config-btn2 save-btns" title="Save" >
                        <span class="docs-tooltip" data-toggle="tooltip">
                          <span class="fa fa-save fa-lg"></span>
                        </span>
                    </button>

                    <a href="<?php echo base_url(); ?>join/" class="btn-red small">Join Again</a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-1 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 text-center share">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <a href="#/" id="facebook_share" class="facebook pull-right" rel="<?php echo ucwords($details['name']); ?>">Share</a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <a href="#/" id="twitter_share" class="twitter pull-left" rel="http://palmolivestayvibrant.com.ph/gallery/page?u=<?php echo $entry_id; ?>">Tweet</a>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of Main Content-->