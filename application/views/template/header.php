<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta property="fb:app_id" content="869124233201793" />
    <meta property="og:title" content="Palmolive Stay Vibrant" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://palmolivestayvibrant.com.ph/" />
    <meta property="og:caption" content="Find the perfect hair color for you. Join the Palmolive Stay Vibrant Promo." />
    <meta property="og:image" content="http://palmolivestayvibrant.com/assets/share_image/facebook-general-janine.jpg?gr=312312312" />
    <meta property="og:description" content="Choose a color and show us your new look for a chance to win a hair coloring session and be featured in a magazine with Janine!" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom-upload.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/cropper/cropper.css">
    <script type="text/javascript">
        var base_url = '<?php echo base_url();?>';
        var rand_num = <?php echo rand(25,1232);?>;
    </script>
    <title></title>
</head>
<body>
    <header>
        <!--Navigation-->
        <div class="navbar navbar-default">
            <nav class="container">
                <div class="navbar-header">
                    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="navbarCollapse" class="collapse navbar-collapse">
	                <ul class="nav navbar-nav">
	                <?php
                		$active = "";
	                	$nav = array('join', 'mechanics', 'gallery', 'videos', 'about-us');
	                	foreach ($nav as $key => $value) {
	                		$str_status = ($value === $this->uri->segment(1)) ? true : false;
	                		if ($str_status) {
	                			$active = "active";
	                		}
	                		$strip_words = str_replace('-', ' ', $value);
	                ?>
		                	<li>
		                		<a class="<?php echo $active; ?>" href="<?php echo base_url().$value; ?>"><?php echo strtoupper($strip_words); ?></a>
		                	</li>
	              	<?php
	              			$active = "";
	                	}
	                ?>
 					</ul>
                
                </div>
            </nav>
        </div>
        <img src="<?php echo base_url(); ?>assets/images/img-header.jpg" alt="" class="img-responsive hidden-xs">
        <!--Mobile Header-->

        <div class="container-fluid hidden-sm hidden-md hidden-lg BGdefault">
           

        <?php 
            if($this->uri->segment(1) == 'join' || $this->uri->segment(1) == ''){ 
                if(!isset($_COOKIE["removeSlideDown"])){
        ?>
            <!--Guide-->
            <div class="row">
                <div id="howTo" class="col-xs-10 col-xs-offset-1 alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close" id="closeSlide">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="col-xs-10 col-xs-offset-1 body text-center">
                        <h5>Swipe down to start your vibrantly colorful transformation</h5>
                    </div>
                </div>
            </div>
            <!--End of Guide-->
        <?php 
                }
            } 
        ?>
            
            <div class="mob-header">
                <div class="col-xs-8 col-xs-push-2">
                    <div class="row">
                        <img src="<?php echo base_url(); ?>assets/images/img-StayVibrant-logo.png" alt="" class="img-responsive">
                    </div>
                </div>
                <div class="row mob-head-content">
                    <div class="col-xs-6 pull-left janine"></div>
                    <div class="col-xs-5 col-xs-pull-1 pull-right content">
                        <h3>Still looking for the perfect hair color?</h3>
                        <p>Choose a color and show us your new look for a chance to win a hair coloring session and be featured in a magazine with Janine!</p>
                    </div>
                </div>
            </div>
            <div class="row BGdefault product-shot">
                <div class="col-xs-6 col-xs-push-3">
                    <img src="<?php echo base_url(); ?>assets/images/img-product.png" alt="" class="img-responsive">
                </div>
            </div>
        </div>
        <!--End of Mobile Header-->        
    </header>

    <!--Strip-->
    <div class="container-fluid strip hidden-xs">
        <div class="container">
            <h3 class="col-md-12">Still looking for the perfect hair color?</h3>
            <h6 class="col-md-8 col-md-offset-2">Choose a color and show us your new look for a chance to win a hair coloring session and be featured in a magazine with Janine!</h6>
        </div>
    </div>