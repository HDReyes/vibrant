    <!--Footer-->
    <footer class="container-fluid navbar-fixed-bottoms"><!--fix-->
        <div class="row">
            <div class="container">
                <span>
                    <a href="https://www.facebook.com/PalmoliveNaturals/?fref=ts" target="_blank" class="facebook">Facebook</a>
                    <a href="https://www.youtube.com/user/PalmoliveNaturalsPh" target="_blank" class="youtube">Youtube</a>
                </span>
                <p class="col-md-12">Palmolive Naturals Philippines 2016 Copyright</p>
            </div>
        </div>
    </footer>

    <!--Scripts-->

<script src="<?php echo base_url(); ?>assets/js/image-orient/load-image.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image-orient/load-image-meta.js"></script>

    
<!--
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=1704253839786107";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
-->    
    <div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '869124233201793',
      xfbml      : true,
      version    : 'v2.5'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

    <script src="<?php echo base_url(); ?>assets/js/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/dist/jquery.validate.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/blazy.min.js"></script>
    <!--
    <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    -->
    <script src="<?php echo base_url(); ?>assets/js/Promise.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/cropper/cropper.min.js"></script>
    <!--
    <script src="<?php echo base_url(); ?>assets/js/exif.js"></script>
    -->
    <!--
    <script src="<?php echo base_url(); ?>assets/js/load-image.all.min.js"></script>
    -->



    <?php if($this->uri->segment(3) == 'sample'):?>
            <script src="<?php echo base_url(); ?>assets/js/canvas/upload.js?r=<?php echo rand(5,300);?>"></script>
            <script src="<?php echo base_url(); ?>assets/js/canvas/rotate.js?r=<?php echo rand(5,300);?>"></script>
            <script src="<?php echo base_url(); ?>assets/js/canvas/cropper.js?r=<?php echo rand(5,300);?>"></script>
            <script src="<?php echo base_url(); ?>assets/js/canvas/fill-out.js?r=<?php echo rand(5,300);?>"></script>
    <?php else:?>
        <script src="<?php echo base_url(); ?>assets/js/canvas/upload.js?r=<?php echo rand(5,300);?>"></script>
        <script src="<?php echo base_url(); ?>assets/js/canvas/rotate.js?r=<?php echo rand(5,300);?>"></script>
        <script src="<?php echo base_url(); ?>assets/js/canvas/cropper.js?r=<?php echo rand(5,300);?>"></script>

        <script src="<?php echo base_url(); ?>assets/js/canvas/sketch.js?r=<?php echo rand(5,300);?>"></script>
        <?php /*<script src="<?php echo base_url(); ?>assets/js/orientation.js"></script>*/?>
    <?php endif;?>
    
    <script src="<?php echo base_url(); ?>assets/js/jquery.masked.input.js"></script>

    <script>
		(function($){
			$(window).load(function(){
				$("#scroll-it-gallery-main").mCustomScrollbar({
					theme:"rounded",
                    advanced:{ updateOnContentResize: true },
                    advanced:{ updateOnImageLoad: true },
                    callbacks:{
                        onTotalScroll:function(){
                            console.log("Scrolled to end of content.");
                            var gal_count = $('#gallery_count').val();

                          //alert('Current Count: '+gal_count);
                           //$(".gallery-list ul").append("<li class='col-md-4 col-sm-6 col-xs-6'><div class='col-md-12 text-center userImage2 effect'><img src='ntries/img-user06.jpg' alt=''><strong class='col-md-12 userName'>Joanna Dela Cruz</strong></div></li>");
                            var loaderContainer = $('.loader');
                            var img = jQuery("<img class='imageLoader' src='<?php echo base_url(); ?>assets/images/7152.GIF' style='align:center; margin:0px 0px 10px 0px; height:30px; width:30px;'>");                      
                            loaderContainer.prepend(img);

                           $.ajax({
                              method: "POST",
                              url: "<?php echo base_url(); ?>gallery/fetch_gallery/",
                              data: { from: gal_count, ajax: 1 },
                              success: function(data)
                              {
                                    console.log(data);
                                    setTimeout(function() {
                                      //$('.message').remove();
                                      img.remove();
                                    }, 2000);

                                    if (data['status']) {


                                        //gal_count += Number(data['gallery_data'].length);
                                        //if (gal_count <= data['gallery_data'].length) {
                                            //img.remove();

                                            // Sums up the total row up this point 
                                            $('#gallery_count').val(Number(data['gallery_data'].length) + Number(gal_count));
                                            for (var i = 0; i < data['gallery_data'].length; i++) {
                                                var name  = data['gallery_data'][i]['name'].split(" ");
                                                var fname = name[0];
                                                var lname = name[1];                                                
                                                //data['gallery_data'][i]['']
                                                //alert('Return data: ' + data['gallery_data'][i]['name'])
                                                $(".gallery-list ul").append("<li class='col-md-4 col-sm-6 col-xs-6'><a class='col-md-12 text-center userImage2 effect' data-toggle='modal' data-target='#" + data['gallery_data'][i]['entry_id'] + "' href='#' onclick='return false'><img src='<?php echo base_url(); ?>entries/" + data['gallery_data'][i]['image_name'] + "' alt=''><strong class='col-md-12 userName'>" + fname + "</strong></a><div id='" + data['gallery_data'][i]['entry_id'] + "' class='modal fade' role='dialog' aria-hidden='true'><div class='modal-dialog'><div class='modal-content col-xs-8 col-xs-offset-2'><div class='modal-header row'><h5 class='col-xs-8 paddingL-0'>" + data['gallery_data'][i]['name'] + "<h5><button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button></div><div class='modal-body row'><img src='<?php echo base_url(); ?>entries/" + data['gallery_data'][i]['image_name'] + "' alt='' class=''></div></div></div></div></li>");

                                            };
                                            


                                        //};
                                    };
                              },error: function(e){
                                    console.log(e);
                              }                                                            
                            });



                        }
                    }
				});

        $("#scroll-it-gallery-sub").mCustomScrollbar({
          theme:"rounded",
                    advanced:{ updateOnContentResize: true },
                    advanced:{ updateOnImageLoad: true },
                    callbacks:{
                        onTotalScroll:function(){
                            console.log("Scrolled to end of content.");
                            var gal_count = $('#gallery_count').val();

                          //alert('Current Count: '+gal_count);
                           //$(".gallery-list ul").append("<li class='col-md-4 col-sm-6 col-xs-6'><div class='col-md-12 text-center userImage2 effect'><img src='ntries/img-user06.jpg' alt=''><strong class='col-md-12 userName'>Joanna Dela Cruz</strong></div></li>");
                            var loaderContainer = $('.loader');
                            var img = jQuery("<img class='imageLoader' src='<?php echo base_url(); ?>assets/images/7152.GIF' style='align:center; margin:0px 0px 10px 0px; height:30px; width:30px;'>");                      
                            loaderContainer.prepend(img);

                           $.ajax({
                              method: "POST",
                              url: "<?php echo base_url(); ?>gallery/fetch_gallery/",
                              data: { from: gal_count, ajax: 1 },
                              success: function(data)
                              {
                                    console.log(data);
                                    setTimeout(function() {
                                      //$('.message').remove();
                                      img.remove();
                                    }, 2000);

                                    if (data['status']) {


                                        //gal_count += Number(data['gallery_data'].length);
                                        //if (gal_count <= data['gallery_data'].length) {
                                            //img.remove();

                                            // Sums up the total row up this point 
                                            $('#gallery_count').val(Number(data['gallery_data'].length) + Number(gal_count));
                                            for (var i = 0; i < data['gallery_data'].length; i++) {
                                                var name  = data['gallery_data'][i]['name'].split(" ");
                                                var fname = name[0];
                                                var lname = name[1];                                                
                                                //data['gallery_data'][i]['']
                                                //alert('Return data: ' + data['gallery_data'][i]['name'])
                                                //$(".gallery-list ul").append("<li class='col-md-4 col-sm-6 col-xs-6'><a class='col-md-12 text-center userImage2 effect' href=''><img src='<?php echo base_url(); ?>entries/" + data['gallery_data'][i]['image_name'] + "' alt=''><strong class='col-md-12 userName'>" + fname + "</strong></a></li>");
                                                $("#scroll-it-gallery-sub ul").append("<li class='col-xs-4'><a class='row text-center userImage2 effect' data-toggle='modal' data-target='#" + data['gallery_data'][i]['entry_id'] + "' href='#' onclick='return false'><img src='<?php echo base_url(); ?>entries/" + data['gallery_data'][i]['image_name'] + "' alt=''><strong class='col-md-12 userName'>" + fname + "</strong></a><div id='" + data['gallery_data'][i]['entry_id'] + "' class='modal fade' role='dialog' aria-hidden='true'><div class='modal-dialog'><div class='modal-content col-xs-8 col-xs-offset-2'><div class='modal-header row'><h5 class='col-xs-8 paddingL-0'>" + data['gallery_data'][i]['name'] + "<h5><button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button></div><div class='modal-body row'><img src='<?php echo base_url(); ?>entries/" + data['gallery_data'][i]['image_name'] + "' alt='' class=''></div></div></div></div></li>");


                                            };
                                            


                                        //};
                                    };
                              },error: function(e){
                                    console.log(e);
                              }                                                            
                            });



                        }
                    }
        });


                $("#scroll-it-color").mCustomScrollbar({
                    theme:"rounded",
                });
                $(".scroll-it-mechanics").mCustomScrollbar({
                    theme:"rounded",
                });
                $('#twitter_share').click(function (e) {
                    //e.preventDefault();
                    //var loc = "http://bit.ly/1SvNimm";
                    var url = $(this).attr('rel');

                    var loc = url; //"http://tinyurl.com/stayvibrant";
                    var title  = "I found the hair color that’s so me with %23PalmoliveStayVibrant! Try it for a chance to win prizes! ";
                    window.open('http://twitter.com/share?url=' + loc + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
                });

                $('#facebook_share').click(function (e) {

                    // FB.ui({
                    //   method: 'share',
                    //   href: 'http://palmolivevibrant.com',
                    // }, function(response){});
                      var uname = $(this).attr('rel');
                      FB.ui({
                          
                          // method: 'share',
                          // href: 'http://fbappbox.com/prototype_nescafe/index.php',
                          method: 'feed',
                          link: 'http://palmolivestayvibrant.com',
                          description: 'Thank you for joining the Palmolive Stay Vibrant. Have a great hair day!',
                          name: uname + ' is having a great hair day!',
                          caption: 'Pick-out the best hair color for you, join the Palmolive Stay Vibrant promo.',

                          //picture: 'http://palmolivestayvibrant.com/assets/share_image/FB-share-image-mockup.jpg?gr=312312312',
                         
                          picture: 'http://resources2.news.com.au/images/2014/03/20/1226860/322910-60cf9794-afdc-11e3-bf6a-6ec13e402258.jpg',

                          display: 'popup',
                          app_id: '869124233201793',

                        }, function(response){

                            console.log(response);

                        });                    

                });

                $("#scroll-it-video").mCustomScrollbar({
                    theme:"rounded",
                });

                $('.vid-thumb').click(function (e) {
                    var vid = $(this).attr('rel');
                    // alert($(this).attr('rel'));
                    // var loaderContainer = $('.embed-responsive');
                    // var img = jQuery("<img class='imageLoader' src='<?php echo base_url(); ?>assets/images/7152.GIF' style='align:center; margin:0px 0px 10px 0px; height:30px; width:30px;'>");                      
                    // loaderContainer.prepend(img);

                   $.ajax({
                      method: "POST",
                      url: "<?php echo base_url(); ?>videos/fetch_video/",
                      data: { vid_id: vid, ajax: 1  },
                      success: function(data)
                      {
                            // setTimeout(function() {
                            //   //$('.message').remove();
                            //   //img.remove();
                            // }, 2000);
                            console.log(data['status']);
                            if (data['status']) {
                                
                                //gal_count += Number(data['gallery_data'].length);
                                //if (gal_count <= data['gallery_data'].length) {
                                    //img.remove();

                                    // Sums up the total row up this point 
                                    // $('#gallery_count').val(Number(data['gallery_data'].length) + Number(gal_count));
                                    // for (var i = 0; i < data['gallery_data'].length; i++) {
                                    //     //data['gallery_data'][i]['']
                                    //     //alert('Return data: ' + data['gallery_data'][i]['name'])
                                    //     $(".gallery-list ul").append("<li class='col-md-4 col-sm-6 col-xs-6'><div class='col-md-12 text-center userImage2 effect'><img src='<?php echo base_url(); ?>entries/" + data['gallery_data'][i]['image_name'] + "' alt=''><strong class='col-md-12 userName'>" + data['gallery_data'][i]['name'] + "</strong></div></li>");
                                    

                                    // };
                                    $(".embed-responsive").append("<iframe class='embed-responsive-item' src='" + data['video_url'] + "' frameborder='0' allowfullscreen></iframe>");


                                //};
                            };
                      },error: function(e){
                            console.log(e);
                      }                                                            
                    });

                });                
                // $(window).on("scrollstart",function(){
                //     alert("Started scrolling!");
                // }); 
//                  $("#scroll-it").on({
//                     'touchstart': function(e) { 
// //                        console.log($(this).scrollTop()); // Replace this with your code.
//                         if (e.scrollTop() === 0) { // fix scrolling
//                             e.scrollTop(100);
//                         }                        
//                         alert(e.scrollTop());
//                         // if ($("#scroll-it").scrollTop()  > $("#scroll-it").height() / 2)
//                         // {
//                         //     //alert('You are in the middle of the page');
//                         // }                        
//                     }
//                 }); 
//                 $("#scroll-it").on({
//                     'touchmove': function(e) { 
//                         //console.log($(this).scrollTop()); // Replace this with your code.
//                         //alert('d');   
//                         alert($("#scroll-it").scrollTop() + '====' + $("#scroll-it").height() / 4);                     
//                         if ($("#scroll-it").scrollTop()  == $("#scroll-it").height() / 2)
//                         {
//                             alert('You are in the middle of the page' + $(this).scrollTop() + - + $(window).height());
//                         } 

//                     }
//                 }); 

			});

                // $("#scroll-it").scroll(function() {
                //      alert('You are in the middle of the page');
                //     if ($("#scroll-it").scrollTop()  > $("#scroll-it").height() / 2)
                //     {
                //         alert('You are in the middle of the page');
                //     }
                // });

            if ($("#scroll-it-gallery-main").length) {
                   // alert('ss');
                    var gal_count       = $('#gallery_count').val();
                    var loaderContainer = $("#loaderContainerTop");
                    var img             = jQuery("<div style='width:100%; display:block; text-align:center'><img class='imageLoader' src='<?php echo base_url(); ?>assets/images/7152.GIF' style='align:center; margin:0px 0px 10px 0px; height:30px; width:30px;'></div>");                      
                    loaderContainer.html(img);

                   $.ajax({
                      method: "POST",
                      url: "<?php echo base_url(); ?>gallery/fetch_gallery/",
                      data: { from: gal_count, ajax: 1 },
                      success: function(data)
                      {
                            console.log(data);
                            setTimeout(function() {
                              //$('.message').remove();
                              img.remove();
                            }, 2000);

                            $('.imageLoader').hide();

                            if (data['status']) {
                                
                                //gal_count += Number(data['gallery_data'].length);
                                //if (gal_count <= data['gallery_data'].length) {
                                    //img.remove();

                                    // Sums up the total row up this point 
                                    $('#gallery_count').val(Number(data['gallery_data'].length) + Number(gal_count));
                                    for (var i = 0; i < data['gallery_data'].length; i++) {
                                        var name  = data['gallery_data'][i]['name'].split(" ");
                                        var fname = name[0];
                                        var lname = name[1];                                                

                                        //data['gallery_data'][i]['']
                                        //alert('Return data: ' + data['gallery_data'][i]['name'])
                                        $("#scroll-it-gallery-main ul").append("<li class='col-md-4 col-sm-6 col-xs-6'><a class='col-md-12 text-center userImage2 effect' data-toggle='modal' data-target='#" + data['gallery_data'][i]['entry_id'] + "' href='#' onclick='return false'><img src='<?php echo base_url(); ?>entries/" + data['gallery_data'][i]['image_name'] + "' alt=''><strong class='col-md-12 userName'>" + fname + "</strong></a><div id='" + data['gallery_data'][i]['entry_id'] + "' class='modal fade' role='dialog' aria-hidden='true'><div class='modal-dialog'><div class='modal-content col-xs-8 col-xs-offset-2'><div class='modal-header row'><h5 class='col-xs-8 paddingL-0'>" + data['gallery_data'][i]['name'] + "<h5><button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button></div><div class='modal-body row'><img src='<?php echo base_url(); ?>entries/" + data['gallery_data'][i]['image_name'] + "' alt='' class=''></div></div></div></div></li>");

                                    };
                                    


                                //};
                            };
                      },error: function(e){
                            console.log(e);
                            $('.imageLoader').hide();

                      }                                                            
                    });                                                

            }            


            if ($("#scroll-it-gallery-sub").length) {
                    var gal_count       = $('#gallery_count').val();
                    var loaderContainer = $("#loaderContainerTopSub");
                    var img             = jQuery("<div style='width:100%; display:block; text-align:center'><img class='imageLoader' src='<?php echo base_url(); ?>assets/images/7152.GIF' style='align:center; margin:0px 0px 10px 0px; height:30px; width:30px;'></div>");                      
                    loaderContainer.html(img);

                   $.ajax({
                      method: "POST",
                      url: "<?php echo base_url(); ?>gallery/fetch_gallery/",
                      data: { from: gal_count, ajax: 1 },
                      success: function(data)
                      {
                            console.log(data);
                            setTimeout(function() {
                              //$('.message').remove();
                              img.remove();
                            }, 2000);

                            if (data['status']) {
                                
                                //gal_count += Number(data['gallery_data'].length);
                                //if (gal_count <= data['gallery_data'].length) {
                                    //img.remove();

                                    // Sums up the total row up this point 
                                    $('#gallery_count').val(Number(data['gallery_data'].length) + Number(gal_count));
                                    for (var i = 0; i < data['gallery_data'].length; i++) {
                                        var name  = data['gallery_data'][i]['name'].split(" ");
                                        var fname = name[0];
                                        var lname = name[1];                                                

                                        //data['gallery_data'][i]['']
                                        //alert('Return data: ' + data['gallery_data'][i]['name'])
                    // $(".gallery-list ul").append("<li class='col-md-4 col-sm-6 col-xs-6'><a class='col-md-12 text-center userImage2 effect' href=''><img src='<?php echo base_url(); ?>entries/" + data['gallery_data'][i]['image_name'] + "' alt=''><strong class='col-md-12 userName'>" + fname + "</strong></a></li>");

                                        $("#scroll-it-gallery-sub ul").append("<li class='col-xs-4'><a class='row text-center userImage2 effect' data-toggle='modal' data-target='#" + data['gallery_data'][i]['entry_id'] + "' href='#' onclick='return false'><img src='<?php echo base_url(); ?>entries/" + data['gallery_data'][i]['image_name'] + "' alt=''><strong class='col-md-12 userName'>" + fname + "</strong></a><div id='" + data['gallery_data'][i]['entry_id'] + "' class='modal fade' role='dialog' aria-hidden='true'><div class='modal-dialog'><div class='modal-content col-xs-8 col-xs-offset-2'><div class='modal-header row'><h5 class='col-xs-8 paddingL-0'>" + data['gallery_data'][i]['name'] + "<h5><button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button></div><div class='modal-body row'><img src='<?php echo base_url(); ?>entries/" + data['gallery_data'][i]['image_name'] + "' alt='' class=''></div></div></div></div></li>");

                                    };
                                    


                                //};
                            };
                      },error: function(e){
                            console.log(e);
                      }                                                            
                    });                                                

            }     

           $('#closeSlide').click(function (e) {
              document.cookie="removeSlideDown=yes";

           });

           $('#closeHowTo').click(function (e) {
              document.cookie="removeHowTo=yes";

           });           
		})(jQuery);
	</script>
<script>
// only for demo purposes
$.validator.setDefaults({
    submitHandler: function() {
        alert("submitted!");
    }
});

(function($){
    //$('#cbirthdate').autoNumeric('init', {  lZero: 'deny', aSep: '/', mDec: 0 });   
$("#cbirthdate").mask("9999/99/99",{placeholder:"yyyy/mm/dd"});
$("#ccontact").mask("(9999) 999-9999");

$.validator.addMethod("check_date_of_birth", function(value, element) {
    
    var age = 18; 
    var mydate = new Date($("#cbirthdate").val());
    mydate.setFullYear(mydate.getFullYear());
    var currdate = new Date();
    currdate.setFullYear(currdate.getFullYear() - age);

    // if ((currdate - mydate) < 0){
    //     alert("Sorry, only persons over the age of " + age + " may enter this site");
    //     return false;
    // }
    return currdate > mydate;
    // var day = $("#dob_day").val();
    // var month = $("#dob_month").val();
    // var year = $("#dob_year").val();
    // var age =  18;

    // var mydate = new Date();
    // mydate.setFullYear(year, month-1, day);

    // var currdate = new Date();
    // currdate.setFullYear(currdate.getFullYear() - age);

    // return currdate > mydate;

}, "(You must be at least 18 years of age.)");

    var validator = $("#form1").validate({
        errorPlacement: function(error, element) {
            //var modWidth = '100%';
            // Append error within linked label
            // $( element )
            //     .closest( "form" )
            //         .find( "label[for='" + element.attr( "id" ) + "']" )
            //             .append( error );
            // $( element )
            //     .closest( "form" )
            //         .find( "label[for='" + element.attr( "id" ) + "']" )
            //             .attr( "style", "width:100%" );
            error.insertBefore("#" + element.attr( "id" ));
            //console.log(element);            

        },
        errorElement: "div",
        rules: {
            email: {
                required: true,
                email: true
            },
            name: {
                required: true,
                minlength: 3
            },
            address: {
                required: true,
                minlength: 3
            },
            contact: {
                required: true,
                //digits: true,
                minlength: 6,
                maxlength: 15
            },
            birthdate: {
              required: true,
              dateISO: true,
              check_date_of_birth: true
            },
        },        
        messages: {
            name: {
                required: " (let us get to know you better)",
                minlength: " (enter a valid name)"
            },
            address: {
                required: " (let us get to know you better)",
                minlength: " (enter a valid address)",
            },
            contact: {
                required: " (let us get to know you better)",
                minlength: " (enter a valid contact number)",
                maxlength: " (enter a valid contact number)",
                digit: " (enter only numbers)",
            },
            birthdate: {
                required: " (let us get to know you better)",
                dateISO: " (please follow the valid birthdate format)",
            },

            email: "(please enter a valid email address)",

        },
        submitHandler: function(form) {
            // do other things for a valid form
            form.submit();
        },
        success: function(label,element) {
           //label.parent().removeClass('error');
           //console.log(label);
           //label.remove(); 
           var modWidth = '0%';
           //console.log(label.parent());
           console.log(label[0].id);
           //label.parent().removeAttr("style");
           // $( element )
           //      .closest( "form" )
           //          .find( label[0].id )
           //          .parent()
           //              .remove();
           console.log($( "#form1" ).find( "#" + label[0].id ));
           $( "#form1" ).find( "#" + label[0].id ).remove();
        }
        // ,
        // invalidHandler: function(event, validator) {
        //     // 'this' refers to the form
        //     var errors = validator.numberOfInvalids();
        //     if (errors) {
        //         console.log(errors);
        //       var message = errors == 1
        //         ? 'You missed 1 field. It has been highlighted'
        //         : 'You missed ' + errors + ' fields. They have been highlighted';
        //       $("div.error span").html(message);
        //       $("div.error").show();
        //     } else {
        //       $("div.error").hide();
        //     }
        // }
    });





})(jQuery);
</script>  
<script type="text/javascript">
        ;(function() {
            // Initialize
            var bLazy = new Blazy({ container: '#scroll-it' });
        })();

</script>
</body>
</html>