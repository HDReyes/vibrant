    <!--Footer-->
    <footer class="container-fluid navbar-fixed-bottoms"><!--fix-->
        <div class="row">
            <div class="container">
                <span>
                    <a href="https://www.facebook.com/PalmoliveNaturals/?fref=ts" target="_blank" class="facebook">Facebook</a>
                    <a href="https://www.youtube.com/user/PalmoliveNaturalsPh" target="_blank" class="youtube">Youtube</a>
                </span>
                <p class="col-md-12">Palmolive Naturals Philippines 2016 Copyright</p>
            </div>
        </div>
    </footer>

    <!--Scripts-->

<script src="<?php echo base_url(); ?>assets/js/image-orient/load-image.js"></script>
<script src="<?php echo base_url(); ?>assets/js/image-orient/load-image-meta.js"></script>

    
<!--
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=1704253839786107";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
-->    
    <div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '869124233201793',
      xfbml      : true,
      version    : 'v2.5'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

    <script src="<?php echo base_url(); ?>assets/js/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/dist/jquery.validate.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/blazy.min.js"></script>
    <!--
    <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    -->
    <script src="<?php echo base_url(); ?>assets/js/Promise.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/cropper/cropper.min.js"></script>

    <!--
    <script src="<?php echo base_url(); ?>assets/js/exif.js"></script>
    -->
    <!--
    <script src="<?php echo base_url(); ?>assets/js/load-image.all.min.js"></script>
    -->



    <?php if($this->uri->segment(3) == 'sample'):?>
            <script src="<?php echo base_url(); ?>assets/js/canvas/upload.js?r=<?php echo rand(5,300);?>"></script>
            <script src="<?php echo base_url(); ?>assets/js/canvas/rotate.js?r=<?php echo rand(5,300);?>"></script>
            <script src="<?php echo base_url(); ?>assets/js/canvas/cropper.js?r=<?php echo rand(5,300);?>"></script>
            <script src="<?php echo base_url(); ?>assets/js/canvas/fill-out.js?r=<?php echo rand(5,300);?>"></script>
    <?php else:?>
        <script src="<?php echo base_url(); ?>assets/js/canvas/upload.js?r=<?php echo rand(5,300);?>"></script>
        <script src="<?php echo base_url(); ?>assets/js/canvas/rotate.js?r=<?php echo rand(5,300);?>"></script>
        <script src="<?php echo base_url(); ?>assets/js/canvas/cropper.js?r=<?php echo rand(5,300);?>"></script>

        <script src="<?php echo base_url(); ?>assets/js/canvas/sketch.js?r=<?php echo rand(5,300);?>"></script>
        <?php /*<script src="<?php echo base_url(); ?>assets/js/orientation.js"></script>*/?>
    <?php endif;?>
    
    <script src="<?php echo base_url(); ?>assets/js/jquery.masked.input.js"></script>
    
    <script src="<?php echo base_url(); ?>assets/js/gallery.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/sharing.js"></script>

    <script>
		(function($){

      $("#final_img").load(function(){
        $("#process_img_text").hide();
      });

			$(window).load(function(){
                $("#scroll-it-color").mCustomScrollbar({
                    theme:"rounded",
                });
                $(".scroll-it-mechanics").mCustomScrollbar({
                    theme:"rounded",
                });

                $("#scroll-it-video").mCustomScrollbar({
                    theme:"rounded",
                });

                 $('#closeSlide').click(function (e) {
                    document.cookie="removeSlideDown=yes";

                 });

                 $('#closeHowTo').click(function (e) {
                    document.cookie="removeHowTo=yes";

                 });
                $('.vid-thumb').click(function (e) {
                    var vid = $(this).attr('rel');
                   $.ajax({
                      method: "POST",
                      url: "<?php echo base_url(); ?>videos/fetch_video/",
                      data: { vid_id: vid, ajax: 1  },
                      success: function(data)
                      {
                            console.log(data['status']);
                            if (data['status']) {
                                
                                    $(".embed-responsive").append("<iframe class='embed-responsive-item' src='" + data['video_url'] + "' frameborder='0' allowfullscreen></iframe>");
                            };
                      },error: function(e){
                            console.log(e);
                      }                                                            
                    });

                });                
			});


           
		})(jQuery);


    function testing()
    {
      alert('aaaa');
    }
	</script>
<script>
// only for demo purposes
$.validator.setDefaults({
    submitHandler: function() {
        alert("submitted!");
    }
});

(function($){
    //$('#cbirthdate').autoNumeric('init', {  lZero: 'deny', aSep: '/', mDec: 0 });   
$("#cbirthdate").mask("9999/99/99",{placeholder:"yyyy/mm/dd"});
$("#ccontact").mask("(9999) 999-9999");

$.validator.addMethod("check_date_of_birth", function(value, element) {
    
    var age = 18; 
    var mydate = new Date($("#cbirthdate").val());
    mydate.setFullYear(mydate.getFullYear());
    var currdate = new Date();
    currdate.setFullYear(currdate.getFullYear() - age);

    // if ((currdate - mydate) < 0){
    //     alert("Sorry, only persons over the age of " + age + " may enter this site");
    //     return false;
    // }
    return currdate > mydate;
    // var day = $("#dob_day").val();
    // var month = $("#dob_month").val();
    // var year = $("#dob_year").val();
    // var age =  18;

    // var mydate = new Date();
    // mydate.setFullYear(year, month-1, day);

    // var currdate = new Date();
    // currdate.setFullYear(currdate.getFullYear() - age);

    // return currdate > mydate;

}, "(You must be at least 18 years of age.)");

    var validator = $("#form1").validate({
        errorPlacement: function(error, element) {
            //var modWidth = '100%';
            // Append error within linked label
            // $( element )
            //     .closest( "form" )
            //         .find( "label[for='" + element.attr( "id" ) + "']" )
            //             .append( error );
            // $( element )
            //     .closest( "form" )
            //         .find( "label[for='" + element.attr( "id" ) + "']" )
            //             .attr( "style", "width:100%" );
            error.insertBefore("#" + element.attr( "id" ));
            //console.log(element);            

        },
        errorElement: "div",
        rules: {
            email: {
                required: true,
                email: true
            },
            name: {
                required: true,
                minlength: 3
            },
            address: {
                required: true,
                minlength: 3
            },
            contact: {
                required: true,
                //digits: true,
                minlength: 6,
                maxlength: 15
            },
            birthdate: {
              required: true,
              dateISO: true,
              check_date_of_birth: true
            },
        },        
        messages: {
            name: {
                required: " (let us get to know you better)",
                minlength: " (enter a valid name)"
            },
            address: {
                required: " (let us get to know you better)",
                minlength: " (enter a valid address)",
            },
            contact: {
                required: " (let us get to know you better)",
                minlength: " (enter a valid contact number)",
                maxlength: " (enter a valid contact number)",
                digit: " (enter only numbers)",
            },
            birthdate: {
                required: " (let us get to know you better)",
                dateISO: " (please follow the valid birthdate format)",
            },

            email: "(please enter a valid email address)",

        },
        submitHandler: function(form) {
            // do other things for a valid form
            form.submit();
        },
        success: function(label,element) {
           //label.parent().removeClass('error');
           //console.log(label);
           //label.remove(); 
           var modWidth = '0%';
           //console.log(label.parent());
           console.log(label[0].id);
           //label.parent().removeAttr("style");
           // $( element )
           //      .closest( "form" )
           //          .find( label[0].id )
           //          .parent()
           //              .remove();
           console.log($( "#form1" ).find( "#" + label[0].id ));
           $( "#form1" ).find( "#" + label[0].id ).remove();
        }
        // ,
        // invalidHandler: function(event, validator) {
        //     // 'this' refers to the form
        //     var errors = validator.numberOfInvalids();
        //     if (errors) {
        //         console.log(errors);
        //       var message = errors == 1
        //         ? 'You missed 1 field. It has been highlighted'
        //         : 'You missed ' + errors + ' fields. They have been highlighted';
        //       $("div.error span").html(message);
        //       $("div.error").show();
        //     } else {
        //       $("div.error").hide();
        //     }
        // }
    });





})(jQuery);
</script>  
<script type="text/javascript">
        ;(function() {
            // Initialize
            var bLazy = new Blazy({ container: '#scroll-it' });
        })();

</script>
</body>
</html>