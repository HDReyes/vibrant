    <!--Main Content-->
    <div class="container-fluid main-content thank-you">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 text-center userImage2 effect">
                    <img src="<?php echo base_url(); ?>entries/<?php echo $account_details[0]['image_name']; ?>" alt="">
                    <strong class="col-md-12 userName"><?php echo ucwords($account_details[0]['name']); ?></strong>
                </div>
            </div>


            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 text-center share">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <a href="#/" class="facebook pull-right facebook_share_profile" data-eid="<?php echo $account_details[0]['entry_id'];?>" rel="<?php echo ucwords($account_details[0]['image_name']); ?>">Share</a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <a href="#/" class="twitter pull-left twitter_share_profile" rel="<?php echo $account_details[0]['entry_id']; ?>">Tweet</a>
                        </div>  
                    </div>
                </div>
            </div>

            <div class="row text-center">
                <a href="<?php echo base_url(); ?>join/" class="btn-red small">Join</a>
            </div>
        </div>
    </div>
    <!--End of Main Content-->