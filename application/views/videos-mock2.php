<style type="text/css">
.userImage2 {
    background-color: none !important;
    padding: 0px !important;
    position: relative;
    margin-bottom: 10px !important;
}
</style>
    <!--Main Content-->
    <div class="container-fluid main-content videos">
        <div class="container">
            <h1>Videos</h1>
            <div class="row video-list scrollable" style="height:auto !important;">

                <div class="col-md-8 col-md-offset-2" style="width:95%">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/6bSsmyG9PJE" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>

                </div>
            </div>
            <div class="row video-list scrollable" id="scroll-it-video">
            <ul>
                <li class="col-md-4 col-sm-6 col-xs-6">
                    <div class="col-md-12 text-center userImage2">
                        <img src="<?php echo base_url(); ?>assets/images/vid-mock-1.png" alt="">
                    </div>
                </li>
                <li class="col-md-4 col-sm-6 col-xs-6">
                    <div class="col-md-12 text-center userImage2">
                        <img src="<?php echo base_url(); ?>assets/images/vid-mock-2.png" alt="">
                    </div>
                </li>
                <li class="col-md-4 col-sm-6 col-xs-6">
                    <div class="col-md-12 text-center userImage2">
                        <img src="<?php echo base_url(); ?>assets/images/vid-mock-1.png" alt="">
                    </div>
                </li>
                <li class="col-md-4 col-sm-6 col-xs-6">
                    <div class="col-md-12 text-center userImage2">
                        <img src="<?php echo base_url(); ?>assets/images/vid-mock-2.png" alt="">
                    </div>
                </li>
                <li class="col-md-4 col-sm-6 col-xs-6">
                    <div class="col-md-12 text-center userImage2">
                        <img src="<?php echo base_url(); ?>assets/images/vid-mock-1.png" alt="">
                    </div>
                </li>
                <li class="col-md-4 col-sm-6 col-xs-6">
                    <div class="col-md-12 text-center userImage2">
                        <img src="<?php echo base_url(); ?>assets/images/vid-mock-2.png" alt="">
                    </div>
                </li>
            </ul>
            </div>
            
            <div class="row">
                <div class="col-md-2 col-md-offset-5 col-sm-4 col-sm-offset-4">
                    <a href="<?php echo base_url(); ?>join" class="btn-red large">Join</a>
                </div>
            </div>
        </div>
    </div>
    <!--End of Main Content-->