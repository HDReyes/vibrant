<!DOCTYPE html>
<html>
    
    <head>
      	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/croppie.css">

		<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.2.min.js"></script>


		<script src="<?php echo base_url(); ?>assets/js/croppie.js"></script>

    </head>

    <body>

		<input type="file" id="input-file" name="input-file"  style="display:none"/>

		<input type="button" id="sel_img_btn" name="sel_img_btn" value="Select" />

		<input type="button" id="crop-btn" name="crop-btn" value="Crop" />


		<div id="demo-basic"></div>

		<script type="text/javascript">

			var outlineImage = new Image();

			$(document).ready(function()
			{
				$("#sel_img_btn").on('click', function()
				{
					console.log('test');

					$("#input-file").trigger('click');
				});

				$("#input-file").change(function(){
					previewURL(this);
				});
			});


			function previewURL(input)
		    {
		        if(input.files && input.files[0])
		        {
		            var reader = new FileReader();

		            reader.onload = function(e)
		            {
		                outlineImage.src = e.target.result;
		                console.log(e.target.result);
		            }

		            reader.readAsDataURL(input.files[0]);

		            outlineImage.onload = function()
			        {
			            initCroppingTool();
			        };
		        }
		    }

		    function initCroppingTool()
		    {

			    var basic = $('#demo-basic').croppie({
			        viewport: {
			            width: 150,
			            height: 200
			        }
			    });
			    basic.croppie('bind', {
			        url: outlineImage.src,
			        points: [0,0,280,739]
			    });
			    //on button click
			    //basic.croppie('result', 'html');

			    $("#crop-btn").on('click', function(){
			    	basic.croppie('result', {
		                type : 'canvas',
		                size : 'viewport'
		            }).then(function (resp)
		            {
		            	console.log(resp);
		            });
			    });
			    

		    }
		</script>


		<?php /*<div id="demo-basic"></div>
		<script src="<?php echo base_url();?>assets/js/crop.js"></script>
		*/?>

    </body>

</html>