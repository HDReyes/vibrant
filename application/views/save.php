    <!--Main Content-->
    <div class="container-fluid main-content thank-you">
        <div class="container">
            <h1>Thank You!</h1>
            <h1 id="process_img_text">Please wait while we process your image!</h1>
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 text-center userImage2 effect">
                    <img id="final_img" src="<?php echo base_url();?>saved_entries/<?php echo $filename; ?>" alt="">
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 text-center share">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <a href="#/" id="facebook_share" class="facebook pull-right">Share</a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <a href="#/" id="twitter_share" class="twitter pull-left">Tweet</a>
                        </div>  
                    </div>
                </div>
            </div> -->
            <div class="row text-center">
                <a href="<?php echo base_url(); ?>join/" class="btn-red small">Create Again</a>
            </div>
        </div>
    </div>
    <!--End of Main Content-->