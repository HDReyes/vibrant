   <!--Main Content-->
    <div class="container-fluid main-content videos">
        <div class="container">
            <h1>Videos</h1>
            <div class="row video-list">
                <div class="col-md-8 col-md-offset-2">
                   
                    <!--Row 1-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/U_EIF0yrIiM" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zo1z8FofWkA" frameborder="0" allowfullscreen></iframe>
                            </div>
                             <div class="row text-center hidden-sm hidden-md hidden-lg">
                                <a href="https://www.youtube.com/user/PalmoliveNaturalsPh" target="_blank">Watch more videos</a>
                            </div>
                        </div>
                    </div>
                    
                    <!--Row 2-->
<!-- 
                    <div class="row hidden-xs">
                        <div class="col-md-12">
                            <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/tNNNmJwc6hs" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

 -->                    
                </div>
            </div>
            <div class="row text-center">
                <a href="<?php echo base_url(); ?>join" class="btn-red small">Join</a>
            </div>
        </div>
    </div>
    <!--End of Main Content-->

