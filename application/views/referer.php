<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta property="fb:app_id" content="869124233201793" />
    <meta property="og:title" content="Palmolive Stay Vibrant" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://palmolivestayvibrant.com.ph/gallery/r/<?php echo $account_details[0]['entry_id'];?>" />
    <meta property="og:caption" content="Find the perfect hair color for you. Join the Palmolive Stay Vibrant Promo." />
    <meta property="og:image" content="http://palmolivestayvibrant.com/entries/<?php echo $account_details[0]['image_name'];?>" />
    <meta property="og:description" content="I found the hair color that’s so me with #PalmoliveStayVibrant! Try it for a chance to win prizes!" />
</head>
<body>

</body>
</html>