<!-- Main Content -->
<div class="container-fluid main-content gallery">
    <div class="container">
        <h1>Gallery</h1>
        <div class="row gallery-list scrollable hidden-xs" id="scroll-it-gallery-main">
            <div id="loaderContainerTop"></div>
            <ul>

            </ul>
        </div>
        
        <div class="row gallery-list hidden-sm hidden-md hidden-lg" id="scroll-it-gallery-sub">
            <div id="loaderContainerTopSub"></div>

            <ul>

            </ul>
        </div>


        <div class="row">
            <div class="col-md-2 col-md-offset-5 col-sm-4 col-sm-offset-4 col-xs-4 col-xs-offset-4 loader" style="text-align:center"></div>
        </div>
        <div class="row text-center">
                <a href="<?php echo base_url(); ?>join" class="btn-red small">Join</a>
        </div>
    </div>
</div>
<input type="hidden" id="gallery_count" name="gallery_count" value="0">
<!-- End of Main Content -->
