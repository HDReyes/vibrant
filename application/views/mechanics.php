    <!--Main Content-->

    <div class="container-fluid main-content mechanics">
        <br><br>
            <div class="col-md-12">
                <h1>Mechanics</h1>
            </div>

        <div class="container">
            <div class="col-md-12 mechanics-container">
                <div class="scrollable hidden-xs scroll-it-mechanics">            
                    <h6>1. For Participant</h6>
                    <ol class="start">
                        <li>Participant must be at least 18 years old, a female resident of the Philippines and a fan of the Palmolive Naturals Facebook fan page.</li>
                    </ol>
                    <h6>2. How to join</h6>
                    <ol>
                        <li style="list-style: none;">
                            <ol>
                                <li>
                                    <p>To join, participant must upload her photo or take her selfie, choose her preferred hair color from the Palmolive Stay Vibrant microsite and submit her entry through the same.</p>
                                    <ul>
                                        <li>Photo must not exceed 10MB, otherwise the user will be prompted by the microsite to submit a new photo with the correct image size</li>
                                    </ul>
                                </li>
                                <li>
                                    <p>After participant is done submitting her entry through the Palmolive Naturals Stay Vibrant microsite, participant must register in the microsite with the following information:</p>
                                    <ul>
                                        <li>Name</li>
                                        <li>Birthdate</li>
                                        <li>Address</li>
                                        <li>Telephone Number</li>
                                        <li>E-mail address</li>
                                    </ul>
                                </li>
                                <li>
                                    <p>The information given by the participants will be used by the organizers for the activities of this promotion and to notify them of future campaigns of Palmolive Naturals.</p>
                                </li>
                                <li>
                                    <p>User has the option to share their new hair color look unto their respective Facebook and Twitter accounts and will be prompted to log in if needed.</p>
                                </li>
                            </ol>
                        </li>
                    </ol>
                    <ol class="continue">
                        <li>By supplying the above information:
                            <ol>
                                <li>Participant guarantees that she a) freely enters/participates in the promotion, and b) is not contractually prohibited by any person, company or entity from joining this promotion or from being associated with the Palmolive Naturals brand.</li>
                                <li>Participant agrees to receive information about future campaigns of Palmolive Naturals.</li>
                            </ol>
                        </li>
                        <li>One submission is considered one entry. Participant can submit multiple entries but can only win once.</li>
                        <li>All submissions will be screened. Entries must not contain profanity, malicious, seditious, obscene, political and religious comments/content, or statements that are derogatory to Colgate-Palmolive, its products, other people and groups.</li>
                        <li>Only valid entries will be uploaded to the Palmolive Naturals fan page and the Palmolive Stay Vibrant microsite.</li>
                        <li>Organizers reserve the right to disqualify entries in violation of the guidelines of this promotion. Organizers also have the option to take down entries that, based on their discretion, impede the promotion of the brand as well as those that present intellectual property issues. Participants warrant the original creation of their designs/entries. Participants shall indemnify and hold the organizers free and harmless from damages, injury, suit, or any cost or expense by reason of their misrepresentation of facts or warranties and of their violation of the guidelines of this promotion.</li>
                        <li>Organizers are not responsible for late, lost, illegible or incomplete entries. They do not warrant uninterrupted access to the promo by reason of, but not limited to, technical malfunction, misdirected transmissions or failures, internet access problems, etc.</li>
                    </ol>
                    <h6>3. Judging of Winners and Claiming of Prizes</h6>
                    <ol>
                        <li>Judging of Winners
                            <ol>
                                <li>From all the valid entries, the organizers will choose five (5) winners based on the following criteria:
                                    <ul>
                                        <li>Ability to show off vibrant hair and personality – 50%</li>
                                        <li>Creativity in hair color choice – 30%</li>
                                        <li>Overall appeal – 20%</li>
                                    </ul>
                                </li>
                                <li>A representative from FDA will be invited to witness the judging of entries.</li>
                                <li>The decision of the organizers is final and binding. </li>
                            </ol>
                        </li>
                        <li>Prizes
                            <ol>
                                <li>Each winner will receive the following prizes: 
                                    <ul>
                                        <li>Get featured in a  fashion magazine  with Janine Gutierrez</li>
                                        <li>Free hair color service. (Color will be based on their winning entry. However, shade will be subject to change depending on colorist’s recommendation coming from the assessment of the winner’s hair)</li>
                                        <li>Php 3,000 worth of Palmolive Naturals gift pack</li>
                                    </ul>
                                </li>
                            </ol>
                        </li>
                        <li>The organizers will not shoulder the winners’ transportation costs, accommodation and other expenses. This limitation also applies if the winner is residing outside Metro Manila.</li>
                    </ol>
                    <h6>4. Obligations of Winners</h6>
                    <ol>
                        <li>Winners must sign a consent form confirming that:
                            <ol class="decimal">
                                <li>She agrees to have her hair dyed based on colorist’s recommendation coming from the assessment of the winner’s hair.
                                    <ol>
                                        <li>Organizers have the right to replace the winner with the next best entry participant in case the winner is not allowed to color her hair (based on salon's assessment on the quality of participant’s hair)</li>
                                    </ol>
                                </li>
                                <li>She has no allergic reactions to any hair colorant ingredient.
                                    <ol>
                                        <li>Organizers have the right to replace the winner with the next best entry participant should the winner misrepresent or declare allergies to hair colorants.</li>
                                    </ol>
                                </li>
                                <li>She agrees to Colgate-Palmolive’s use of her entry, photos, and behind the scenes footage and revisions / edits thereof.</li>
                            </ol>
                        </li>
                        <li>Winners cannot be contractually prohibited to associate themselves with the Palmolive brand.</li>
                        <li>Winners will also be notified accordingly regarding the details of the rehearsal and fashion shoot.  Winners must signify their confirmation and availability to attend the said activity within seven days from their official receipt of the letter-notification. Organizers reserve the right to replace winner with the next best entry should the winner not be able to participate in the fashion shoot.</li>
                        <li>The winner may be asked to voluntarily participate in Palmolive Naturals activities based on mutually agreeable term / schedule.</li>
                    </ol>
                    <h6>5. Key Dates to Remember: </h6>
                    <ol>
                        <li>January 17, 2016 – April 16, 2016 – Submission of entries</li>
                        <li>April 17 – 24, 2016 – Judging of winners</li>
                        <li>April 25, 2016 – Announcement of winners</li>
                    </ol>
                    <h6>6. Others</h6>
                    <ol>
                        <li>Organizers will contact the shortlisted participants by phone and email for verification of identity. Shortlisted participants must present valid proof of identity.</li>
                        <li>The winners will be announced on the Palmolive Naturals Facebook fan page and notified by registered mail, phone, and by e-mail.</li>
                        <li>Organizers shall determine the date of the fashion shoot in consultation with the winners and Janine with best efforts to consider their availability. If despite best efforts, there is no availability of a winner/s, the organizers have the right to replace the winner with the next best entry.</li>
                        <li>Colgate-Palmolive will not be liable for any cost, medical emergencies and/or injuries that may be encountered or sustained by the participant during the making of her entries, hair coloring session, and during the shoot, or resulting from her misrepresentation of facts / information required herein.</li>
                        <li>Prizes are non-transferable and not convertible to cash.</li>
                        <li>Employees of Colgate-Palmolive, Y&R and their relatives up to the second degree of consanguinity or affinity are not qualified to join the promo.</li>
                        <li>All entries, photos and behind-the-scenes footage of the fashion shoot, edits and revisions thereof shall be owned by Colgate-Palmolive and can be used, edited, published and displayed in any and all advertising materials on Colgate-Palmolive social media sites including Facebook, TV commercial and other advertising channels/for and internal communication materials, at any time without compensation and need for winner's consent. </li>
                        <li>Taxes on prizes, if any, will be shouldered by Colgate-Palmolive.</li>
                        <li>The promotion is in no way sponsored, endorsed or administered by, or associated with, Facebook.</li>
                        <li>The participant is providing information to Colgate-Palmolive and Y&R and not to Facebook. </li>
                        <li>Participant fully releases Facebook for any and all liabilities and injuries that may result from her participation in this promotion.</li>
                    </ol>
                </div>

                <div class="scrollable hidden-sm hidden-md hidden-lg scroll-it-mechanics">
                    <h6>1. For Participant</h6>
                    <ol class="start">
                        <li>Participant must be at least 18 years old, a female resident of the Philippines and a fan of the Palmolive Naturals Facebook fan page.</li>
                    </ol>
                    <h6>2. How to join</h6>
                    <ol>
                        <li style="list-style: none;">
                            <ol>
                                <li>
                                    <p>To join, participant must upload her photo or take her selfie, choose her preferred hair color from the Palmolive Stay Vibrant microsite and submit her entry through the same.</p>
                                    <ul>
                                        <li>Photo must not exceed 10MB, otherwise the user will be prompted by the microsite to submit a new photo with the correct image size</li>
                                    </ul>
                                </li>
                                <li>
                                    <p>After participant is done submitting her entry through the Palmolive Naturals Stay Vibrant microsite, participant must register in the microsite with the following information:</p>
                                    <ul>
                                        <li>Name</li>
                                        <li>Birthdate</li>
                                        <li>Address</li>
                                        <li>Telephone Number</li>
                                        <li>E-mail address</li>
                                    </ul>
                                </li>
                                <li>
                                    <p>The information given by the participants will be used by the organizers for the activities of this promotion and to notify them of future campaigns of Palmolive Naturals.</p>
                                </li>
                                <li>
                                    <p>User has the option to share their new hair color look unto their respective Facebook and Twitter accounts and will be prompted to log in if needed.</p>
                                </li>
                            </ol>
                        </li>
                    </ol>
                    <ol class="continue">
                        <li>By supplying the above information:
                            <ol>
                                <li>Participant guarantees that she a) freely enters/participates in the promotion, and b) is not contractually prohibited by any person, company or entity from joining this promotion or from being associated with the Palmolive Naturals brand.</li>
                                <li>Participant agrees to receive information about future campaigns of Palmolive Naturals.</li>
                            </ol>
                        </li>
                        <li>One submission is considered one entry. Participant can submit multiple entries but can only win once.</li>
                        <li>All submissions will be screened. Entries must not contain profanity, malicious, seditious, obscene, political and religious comments/content, or statements that are derogatory to Colgate-Palmolive, its products, other people and groups.</li>
                        <li>Only valid entries will be uploaded to the Palmolive Naturals fan page and the Palmolive Stay Vibrant microsite.</li>
                        <li>Organizers reserve the right to disqualify entries in violation of the guidelines of this promotion. Organizers also have the option to take down entries that, based on their discretion, impede the promotion of the brand as well as those that present intellectual property issues. Participants warrant the original creation of their designs/entries. Participants shall indemnify and hold the organizers free and harmless from damages, injury, suit, or any cost or expense by reason of their misrepresentation of facts or warranties and of their violation of the guidelines of this promotion.</li>
                        <li>Organizers are not responsible for late, lost, illegible or incomplete entries. They do not warrant uninterrupted access to the promo by reason of, but not limited to, technical malfunction, misdirected transmissions or failures, internet access problems, etc.</li>
                    </ol>
                    <h6>3. Judging of Winners and Claiming of Prizes</h6>
                    <ol>
                        <li>Judging of Winners
                            <ol>
                                <li>From all the valid entries, the organizers will choose five (5) winners based on the following criteria:
                                    <ul>
                                        <li>Ability to show of vibrant hair and personality – 50%</li>
                                        <li>Creativity in hair color choice – 30%</li>
                                        <li>Overall appeal – 20%</li>
                                    </ul>
                                </li>
                                <li>A representative from FDA will be invited to witness the judging of entries.</li>
                                <li>The decision of the organizers is final and binding. </li>
                            </ol>
                        </li>
                        <li>Prizes
                            <ol>
                                <li>Each winner will receive the following prizes: 
                                    <ul>
                                        <li>Get featured in a  fashion magazine  with Janine Gutierrez</li>
                                        <li>Free hair color service. (Color will be based on their winning entry. However, shade will be subject to change depending on colorist’s recommendation coming from the assessment of the winner’s hair)</li>
                                        <li>Php 3,000 worth of Palmolive Naturals gift pack</li>
                                    </ul>
                                </li>
                            </ol>
                        </li>
                        <li>The organizers will not shoulder the winners’ transportation costs, accommodation and other expenses. This limitation also applies if the winner is residing outside Metro Manila.</li>
                    </ol>
                    <h6>4. Obligations of Winners</h6>
                    <ol>
                        <li>Winners must sign a consent form confirming that:
                            <ol class="decimal">
                                <li>She agrees to have her hair dyed based on colorist’s recommendation coming from the assessment of the winner’s hair.
                                    <ol>
                                        <li>Organizers have the right to replace the winner with the next best entry participant in case the winner is not allowed to color her hair (based on salon's assessment on the quality of participant’s hair)</li>
                                    </ol>
                                </li>
                                <li>She has no allergic reactions to any hair colorant ingredient.
                                    <ol>
                                        <li>Organizers have the right to replace the winner with the next best entry participant should the winner misrepresent or declare allergies to hair colorants.</li>
                                    </ol>
                                </li>
                                <li>She agrees to Colgate-Palmolive’s use of her entry, photos, and behind the scenes footage and revisions / edits thereof.</li>
                            </ol>
                        </li>
                        <li>Winners cannot be contractually prohibited to associate themselves with the Palmolive brand.</li>
                        <li>Winners will also be notified accordingly regarding the details of the rehearsal and fashion shoot.  Winners must signify their confirmation and availability to attend the said activity within seven days from their official receipt of the letter-notification. Organizers reserve the right to replace winner with the next best entry should the winner not be able to participate in the fashion shoot.</li>
                        <li>The winner may be asked to voluntarily participate in Palmolive Naturals activities based on mutually agreeable term / schedule.</li>
                    </ol>
                    <h6>5. Key Dates to Remember: </h6>
                    <ol>
                        <li>a.  January 17, 2016 – April 16, 2016 – Submission of entries</li>
                        <li>April 17 – 24, 2016 – Judging of winners</li>
                        <li>April 25, 2016 – Announcement of winners</li>
                    </ol>
                    <h6>6. Others</h6>
                    <ol>
                        <li>Organizers will contact the shortlisted participants by phone and email for verification of identity. Shortlisted participants must present valid proof of identity.</li>
                        <li>The winners will be announced on the Palmolive Naturals Facebook fan page and notified by registered mail, phone, and by e-mail.</li>
                        <li>Organizers shall determine the date of the fashion shoot in consultation with the winners and Janine with best efforts to consider their availability. If despite best efforts, there is no availability of a winner/s, the organizers have the right to replace the winner with the next best entry.</li>
                        <li>Colgate-Palmolive will not be liable for any cost, medical emergencies and/or injuries that may be encountered or sustained by the participant during the making of her entries, hair coloring session, and during the shoot, or resulting from her misrepresentation of facts / information required herein.</li>
                        <li>Prizes are non-transferable and not convertible to cash.</li>
                        <li>Employees of Colgate-Palmolive, Y&R and their relatives up to the second degree of consanguinity or affinity are not qualified to join the promo.</li>
                        <li>All entries, photos and behind-the-scenes footage of the fashion shoot, edits and revisions thereof shall be owned by Colgate-Palmolive and can be used, edited, published and displayed in any and all advertising materials on Colgate-Palmolive social media sites including Facebook, TV commercial and other advertising channels/for and internal communication materials, at any time without compensation and need for winner's consent. </li>
                        <li>Taxes on prizes, if any, will be shouldered by Colgate-Palmolive.</li>
                        <li>The promotion is in no way sponsored, endorsed or administered by, or associated with, Facebook.</li>
                        <li>The participant is providing information to Colgate-Palmolive and Y&R and not to Facebook. </li>
                        <li>Participant fully releases Facebook for any and all liabilities and injuries that may result from her participation in this promotion.</li>
                    </ol>
                </div>                
                <div class="row">
                    <div class="col-md-12" style="text-align:center !important">
                        <p>Per DOH-FDA-CCRR Permit No. 1009 s. 2015. Promo runs from January 17, 2016 to April 16, 2016</p>
                    </div>
                </div>

                <div class="row text-center">
                        <a href="<?php echo base_url(); ?>join/" class="btn-red small">Join</a>
                </div>
            </div>
        </div>
    </div>
