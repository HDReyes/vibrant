    <!--Main Content-->
    <div class="container-fluid main-content about-us">
        <div class="row">
            <div class="col-md-12 about-container">
                <div class="container">
                    <p>
                    Have you always wanted a different hair color, one that’s so you? Play around with the different fabulous hair colors here in the Palmolive Naturals Stay Vibrant website! Find out which color suits you and your personality best.
                    </p>
                       
                    <br>

                    <p>
                    And once you’ve found the best hair color for you, keep it vibrant longer* with Palmolive Naturals Vibrant Color Shampoo and Conditioner! Its formula, infused with 100% natural pomegranate extract, helps protect color-treated hair against fading, dry-out and dullness. Worry less about hair color fading!  Once you've found the best hair color for you, take care of it with the right shampoo and conditioner.
                    </p>
                    <br>
                    <p>
                    Get a chance to win a free hair coloring treatment from us by joining Palmolive Naturals Stay Vibrant Promo now. It’s so easy to join! Simply upload your photo, choose the hair color that best suits you, and submit. Who knows, you might just be one of the lucky winners to be featured in a magazine with Janine Gutierrez. 
                    </p>
                    <br>
                    <p>Rock 2016 with your dream hair color, Palmolive girls!</p>
                    <p style="text-align:left !important; "><small><i>*with daily use; versus non-conditioning shampoo</i></small></p>                
                </div>
            </div>
            <div class="container join">
                <div class="row text-center">
                    <a href="<?php echo base_url(); ?>join" class="btn-red small">Join</a>
                </div>
            </div>
        </div>
        
    </div>    
    <!--End of Main Content-->