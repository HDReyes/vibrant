    
    <style>
        .eraser-on:active { 
            cursor: url(assets/images/eraser-logo.png);
        }

        .brush-on, .brush-on:active, .brush-on:focus
        {
            /*cursor:pointer;*/
            <?php if($this->uri->segment(3) == 'sample'):?>
            cursor:url('<?php echo base_url(); ?>assets/images/img-brush.png'), auto;
            <?php else:?>
            cursor:pointer;
            <?php endif;?>
        }

        .brush-off
        {
            cursor:url('<?php echo base_url(); ?>assets/images/eraser-logo.png'), auto;
        }

        .brush-off-2
        {
            cursor:url('<?php echo base_url(); ?>assets/images/eraser-logo-2.png'), auto;
        }

        .brush-off-3
        {
            cursor:url('<?php echo base_url(); ?>assets/images/eraser-logo-3.png'), auto;
        }
    </style>

    <!--Main Content-->
    <div class="container-fluid main-content upload">
        <?php echo form_open('join/form', 'id="form_editor"');?>
        <input id="upload-button" type="file" class="upload-photo">
        <div class="container" style="position: relative;">
        <?php if(!isset($_COOKIE["removeHowTo"])){ ?>
           <!--Guide-->
            <div class="row">
                <div id="howTo" class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close" id="closeHowTo">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="body">
                        <h2 class="text-center hidden-sm hidden-md hidden-lg">How to</h2>
                        <div class="col-xs-12 col-sm-5 effect" style="background: #fff; padding: 20px 10px 50px 10px">
                            <img src="<?php echo base_url(); ?>assets/images/img-howto.jpg" alt="" class="col-xs-12" style="width: 100%; height: 100%; display: block">
                        </div>
                        
                        <div class="col-xs-12 col-sm-7">
                            <h2 class="hidden-xs">How to</h2>
                            <ol type="1" class="row">
                                <li>Choose hair color</li>
                                <li>Trace the outline of your hair</li>
                                <li>Remove excess colors using the eraser</li>
                                <li>Choose other colors for ombre effect</li>
                            </ol>
                            <h3 class="text-center">Start coloring now!</h3>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of Guide-->
        <?php } ?>
            <div class="row">
                <div style="z-index:100" class="col-md-5 col-sm-5 col-sm-offset-0 col-xs-10 col-xs-offset-1 leftColumn">
                    <!-- tools for mobile devices start-->
                    <!--upload-->
                    <div class="row tools upload-btns photo-options hidden-md hidden-sm hidden-lg">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-6 col-xs-6 left">
                                    <label class="upload-lbl col-md-12 col-sm-12 col-xs-12 btn-red upload">Upload Photo</label>
                                </div>
                                <div class="col-sm-6 col-xs-6 right">
                                    <button type="button" class="webcam-lbl col-md-12 col-sm-12 col-xs-12 btn-red photo">Take a Photo</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--take a photo-->
                    <div class="row tools take-photo-btns photo-options hidden-md hidden-sm hidden-lg">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-6 col-xs-6 left">
                                    <input type="button" class="take-photo-btn col-md-12 col-sm-12 col-xs-12 btn-red photo" value="Take Photo">
                                </div>
                                <div class="col-sm-6 col-xs-6 right">
                                    <input type="button" class="col-md-12 col-sm-12 col-xs-12 pull-right btn-red generic remove-webcam" value="Cancel">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--crop-->
                    <div class="row tools scale-btns photo-options hidden-md hidden-sm hidden-lg">
                        <!--rotate-->
                        <div class="btn-group col-xs-6">
                            <button type="button" class="btn generic upload-config-btns" data-method="rotate" data-option="-90" title="Rotate Left">
                                <span class="docs-tooltip" data-toggle="tooltip">
                                  <span class="fa fa-rotate-left"></span>
                                </span>
                            </button>
                            <button type="button" class="btn generic upload-config-btns" data-method="rotate" data-option="90" title="Rotate Right">
                                <span class="docs-tooltip" data-toggle="tooltip">
                                  <span class="fa fa-rotate-right"></span>
                                </span>
                            </button>
                        </div>
                        <!--zoom-->
                        <div class="btn-group col-xs-6">
                            <button type="button" class="btn generic upload-config-btns" data-method="zoom" data-option="0.1" title="Zoom In">
                                <span class="docs-tooltip" data-toggle="tooltip">
                                  <span class="fa fa-search-plus"></span>
                                </span>
                            </button>
                            <button type="button" class="btn generic upload-config-btns" data-method="zoom" data-option="-0.1" title="Zoom Out">
                                <span class="docs-tooltip" data-toggle="tooltip">
                                  <span class="fa fa-search-minus"></span>
                                </span>
                            </button>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <input type="button" class="col-md-12 col-sm-12 col-xs-12 pull-right btn-red generic crop-btn" value="Continue"> 
                        </div>
                    </div>
                    

                    <!-- tools for mobile devices end-->

                    <!-- placeholder and canvas start-->
                    <div class="row userImage-container">
                        <div id="sketch-cont" class="col-md-12 userImage effect">

                            <div id="loading-screen">
                                <i class="fa fa-circle-o-notch fa-spin"></i>
                            </div>

                            <p class="tip">Trace area to color hair</p>

                            <div id="demo-basic"></div>

                            <img id="placeholder-img" src="<?php echo base_url(); ?>assets/images/img-user-placeholder.jpg" alt="" />
                        </div>
                    </div>
                    <!-- placeholder and canvas end-->
                    
                    <!--brush-->
                    <div class="row tools brush-btns photo-options hidden-md hidden-sm hidden-lg">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="btn-group col-xs-12">
                                    <button type="button" data-bsize="5" class="btn generic upload-config-btns brush-btn" title="Small">
                                        <span class="docs-tooltip" data-toggle="tooltip">
                                          <span class="fa fa-eraser config-eraser-icon-sm"></span>
                                        </span>
                                    </button>
                                    <button type="button" data-bsize="10" class="btn generic upload-config-btns brush-btn brush-active" title="Medium">
                                        <span class="docs-tooltip" data-toggle="tooltip">
                                          <span class="fa fa-eraser config-eraser-icon-md"></span>
                                        </span>
                                    </button>
                                    <button type="button" data-bsize="20" class="btn generic upload-config-btns brush-btn" title="Large">
                                        <span class="docs-tooltip" data-toggle="tooltip">
                                          <span class="fa fa-eraser config-eraser-icon-lg"></span>
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- for desktop devices start-->
                    <!--upload-->
                    <div class="row tools upload-btns photo-options hidden-xs">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 left">
                                    <label class="upload-lbl col-md-12 col-sm-12 col-xs-12 btn-red upload">Upload Photo</label>
                                </div>
                                <div class="col-md-6 col-sm-6 right">
                                    <button type="button" class="webcam-lbl col-md-12 col-sm-12 col-xs-12 btn-red photo">Take a Photo</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--take a photo-->
                    <div class="row tools take-photo-btns photo-options hidden-xs">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-6 col-xs-6 left">
                                    <input type="button" class="take-photo-btn col-md-12 col-sm-12 col-xs-12 btn-red photo" value="Take Photo">
                                </div>
                                <div class="col-sm-6 col-xs-6 right">
                                    <input type="button" class="col-md-12 col-sm-12 col-xs-12 pull-right btn-red generic remove-webcam" value="Cancel">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--crop-->
                    <div class="row tools scale-btns photo-options hidden-xs">
                        <div class="col-md-12">
                            <div class="row">

                                <!--rotate-->
                                <div class="btn-group col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-1">
                                    <button type="button" class="btn generic upload-config-btns" data-method="rotate" data-option="-90" title="Rotate Left">
                                        <span class="docs-tooltip" data-toggle="tooltip">
                                          <span class="fa fa-rotate-left fa-lg"></span>
                                        </span>
                                      </button>
                                      <button type="button" class="btn generic upload-config-btns" data-method="rotate" data-option="90" title="Rotate Right">
                                        <span class="docs-tooltip" data-toggle="tooltip">
                                          <span class="fa fa-rotate-right fa-lg"></span>
                                        </span>
                                      </button>
                                </div>
                                <!--zoom-->
                                <div class="btn-group col-md-5 col-sm-5">
                                    <button type="button" class="btn generic upload-config-btns" data-method="zoom" data-option="0.1" title="Zoom In">
                                        <span class="docs-tooltip" data-toggle="tooltip">
                                          <span class="fa fa-search-plus fa-lg"></span>
                                        </span>
                                      </button>
                                      <button type="button" class="btn generic upload-config-btns" data-method="zoom" data-option="-0.1" title="Zoom Out">
                                        <span class="docs-tooltip" data-toggle="tooltip">
                                          <span class="fa fa-search-minus fa-lg"></span>
                                        </span>
                                      </button>
                                </div>

                                <div class="col-md-12 col-sm-12">
                                    <input type="button" class="col-md-12 col-sm-12 col-xs-12 pull-right btn-red generic crop-btn" value="Continue"> 
                                </div>
                                <!-- <input type="button" class="col-md-12 col-sm-12 col-xs-12 pull-right btn-red generic crop-btn" value="Continue"> -->
                            </div>
                        </div>
                    </div>
                    <!--brush-->
                    <div class="row tools brush-btns photo-options hidden-xs">
                        <div class="btn-group col-md-12">
                            <button type="button" data-bsize="5" class="btn generic upload-config-btns brush-btn" title="Small">
                                <span class="docs-tooltip" data-toggle="tooltip">
                                  <span class="fa fa-eraser config-eraser-icon-sm"></span>
                                </span>
                            </button>
                            <button type="button" data-bsize="10" class="btn generic upload-config-btns brush-btn brush-active" title="Medium">
                                <span class="docs-tooltip" data-toggle="tooltip">
                                  <span class="fa fa-eraser config-eraser-icon-md"></span>
                                </span>
                            </button>
                            <button type="button" data-bsize="20" class="btn generic upload-config-btns brush-btn" title="Large">
                                <span class="docs-tooltip" data-toggle="tooltip">
                                  <span class="fa fa-eraser config-eraser-icon-lg"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <!-- for desktop devices end-->
                </div>
                <div class="col-md-7 col-sm-7 rightColumn">
                    <div class="col-md-12">
                        <div class="row">
                            <button type="button" class="btn-red clear marginR-10">Clear</button>
                            <button type="button" class="btn-red erase">Erase</button>
                        </div>
                    </div>
                    <br clear="all">
                    <div class="row hairColors scrollable" id="scroll-it-color">
                        <div class="col-md-12">
                            <div class="row">
                                <ul class="col-md-12" style="margin-left: 0;">
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="11,7,4,0.4" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_jet-black.jpg" alt="Jet Black">
                                            <strong>Jet Black</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="10,6,5,0.4" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_natural-black.jpg" alt="Natural Black">
                                            <strong>Natural Black</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="60,40,33,0.4" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_dark-brown.jpg" alt="Dark Brown">
                                            <strong>Dark Brown</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="165,108,65,0.3" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_platinum-blonde.jpg" alt="Platinum Blonde">
                                            <strong>Platinum Blonde</strong>
                                        </a>
                                    </li>
                                    
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="0,95,81,0.4" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_mystic-torquiose.jpg" alt="Mystic Torquiose">
                                            <strong>Mystic Torquiose</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="154,40,66,0.3" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_plum.jpg" alt="Plum">
                                            <strong>Plum</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="37,81,146,0.3" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_blue-steel.jpg" alt="Blue Steel">
                                            <strong>Blue Steel</strong>
                                        </a>
                                    </li>
                                    <li  class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="194,1,6,0.3" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_ruby-red.jpg" alt="Ruby Red">
                                            <strong>Ruby Red</strong>
                                        </a>
                                    </li>
                                    
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="204,167,149,0.3" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_rosa-rosa.jpg" alt="Rosa Rosa">
                                            <strong>Rosa Rosa</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="91,4,145,0.4" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_purple-passion.jpg" alt="Purple Passion">
                                            <strong>Purple Passion</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="148,15,98,0.4" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_ultra-violet.jpg" alt="Ultra Violet">
                                            <strong>Ultra Violet</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="195,178,158,0.4" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_vanilla-creme-blonde.jpg" alt="Vanilla Creme Blonde">
                                            <strong>Vanilla Creme Blonde</strong>
                                        </a>
                                    </li>
                                
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="205,30,59,0.4" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_burgundy-bistro.jpg" alt="Burgundy Bistro">
                                            <strong>Burgundy Bistro</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="90,49,29,0.3" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_chocolate-brown.jpg" alt="Chocolate Brown">
                                            <strong>Chocolate Brown</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="158,105,55,0.4" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_golden-brown.jpg" alt="Golden Brown">
                                            <strong>Golden Brown</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="50,117,2,0.3" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_green-grape.jpg" alt="Green Grape">
                                            <strong>Green Grape</strong>
                                        </a>
                                    </li>
                                    
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="106,65,33,0.3" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_light-brown.jpg" alt="Light Brown">
                                            <strong>Light Brown</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="132,74,36,0.4" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_medium-ash-brown.jpg" alt="Medium Ash Brown">
                                            <strong>Medium Ash Brown</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="254,72,113,0.4" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_pink-pearl.jpg" alt="Pink Pearl">
                                            <strong>Pink Pearl</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-3 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="130,36,28,0.4" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_dark-red-copper.jpg" alt="Dark Red Copper">
                                            <strong>Dark Red Copper</strong>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div><!--end:hairColors-->
                    <div class="text-center">
                        <!-- <button type="button" data-option="download" class="btn generic upload-config-btns save-config-btn save-btns" title="Save">
                            <span class="docs-tooltip" data-toggle="tooltip">
                              <span class="fa fa-save fa-lg"></span>
                            </span>
                        </button> -->
                        <input type="button" id="save-btn" value="Submit" data-option="submit" class="btn-red small save-btns">
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close();?>
    </div>
    <!--End of Main Content-->

 