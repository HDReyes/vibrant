<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class MY_Exceptions extends CI_Exceptions 
{



    /**
     * 404 Page Not Found Handler
     *
     * @access  private
     * @param   string
     * @return  string
     */
    public function show_404($page = '', $log_error = TRUE)
    {
        $CI =& get_instance();
        $CI->load->view('custom-error-404');
        echo $CI->output->get_output();
        exit;
    }
    
    public function show_error($heading, $message, $template = 'error_general', $status_code = 500)
    {
        $CI =& get_instance();
        $CI->load->view('custom-server-error');
        echo $CI->output->get_output();
        exit;

    }

}
?>