      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Unique Entry
            <small>This page contains user information with total number of entries...</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Unique Entry</li>
          </ol>
        </section>


        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Users With Unique Entries</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Contact No.</th>
                        <th>Date/Time</th>
                        <th>Total Entries</th>
                      </tr>
                    </thead>
                    <?php 
                      if ($table_data) {
                    ?>
                        <tbody>
                          <?php foreach ($table_data as $key => $value) { ?>
                              <tr>
                                <td><?php echo $value['name']; ?></td>
                                <td><?php echo $value['email']; ?></td>
                                <td><?php echo $value['mobile']; ?></td>
                                <td><?php echo $value['entry_date']; ?></td>
                                <td align="center"><?php echo $value['counttotal']; ?></td>
                              </tr>
                          <?php } ?>
                        </tbody>                    
                    <?php      
                      }
                    ?>
                    <tfoot>
                      <tr>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Contact No.</th>
                        <th>Date/Time</th>
                        <th>Total Entries</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->

      </div><!-- /.content-wrapper -->