<style>
.form1 fieldset p label span.error { color: red; }
/*div.error { display: none; }*/
/*label{ width:100%;}*/
div.error { font-size: 10px; color: red;}
input {	border: 1px solid black; }
input.checkbox { border: none }
input:focus { border: 1px dotted black; }
input.error { border: 1px dotted red; }
form.form1 .gray * { color: gray; }
.error_label{ font-size: 12px; color: red; width: 100%; text-align: center;}
</style>
    <!--Main Content-->
    <div class="container-fluid main-content join">
		<form method="POST" id="form1" action="<?php echo base_url(); ?>join/validate/">
	        <div class="container">
	            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2 join-formContainer effect">
	                <h1>Form</h1>
		        	<?php if ($error_response) { ?>
			        	<label class='error_label'><?=$error_response;?></label>
		        	<?php }?>

<!-- 	                <label for="cname"></label>
 -->	                
 					<input class="col-md-12 col-sm-12 col-xs-12" type="text" id="cname" name="name" placeholder="Name" value="<?=set_value('name')?>" required>
	               	<!-- <label for="caddress"></label> -->
	                <input class="col-md-12 col-sm-12 col-xs-12" type="text" id="caddress" name="address" placeholder="Home Address" value="<?=set_value('address')?>" required>
	               	<!-- <label for="cemail"></label> -->
	                <input class="col-md-12 col-sm-12 col-xs-12" type="text" id="cemail" name="email" placeholder="Email" value="<?=set_value('email')?>" required>
	               	<!-- <label for="ccontact"></label> -->
	                <input class="col-md-12 col-sm-12 col-xs-12" type="text" id="ccontact" name="contact" placeholder="Contact No. (e.g: 091xxxxxxxx)" value="<?=set_value('contact')?>" required>
	               	<!-- <label for="cbirthdate"></label> -->
	                <input class="col-md-12 col-sm-12 col-xs-12" type="text" id="cbirthdate" name="birthdate" placeholder="Birthdate (e.g: yyyy/mm/dd)" value="<?=set_value('birthdate')?>" required>
	                <input type="submit" value="Confirm" class="col-md-4 col-xs-12 pull-right btn-red large marginR-0">
	            </div>
	        </div>
        </form>
    </div>
    <!--End of Main Content-->