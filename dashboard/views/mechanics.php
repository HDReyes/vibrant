   <!--Main Content-->
    <div class="container-fluid main-content mechanics">
        <div class="container">
            <div class="col-md-12 mechanics-container">
                <h1>Stay Vibrant Color Promo Mechanics</h1>
                <ol class="scrollable" id="scroll-it-mechanics">
                    <li>
                        <strong>For Participant</strong>
                        <ol>
                            <li>Participant must be at least 18 years old, a female resident of the Philippines and a fan of the Palmolive Naturals Facebook fan page.</li>
                        </ol>
                    </li>
                    <li>
                        <strong>How to join</strong>
                        <ul>
                            <li>
                                <p>To join, participant must upload her photo or take her selfie, choose her preferred hair color from the Palmolive Stay Vibrant microsite and submit her entry through the same.</p>
                                <ol>
                                    <li>
                                        <p>Photo must not exceed 10MB, otherwise the user will be prompted by the microsite to submit a new photo with the correct image size</p>
                                    </li>
                                </ol>
                            </li>    
                            <li>
                                <p>After participant is done submitting her entry through the Palmolive Naturals Stay Vibrant microsite, participant must register in the microsite with the following information:</p>
                                        <ul>
                                            <li>Name</li>
                                            <li>Age</li>
                                            <li>Address</li>
                                            <li>Telephone Number</li>
                                            <li>E-mail address</li>
                                        </ul>

                            </li>
                            <li>
                                <p>The information given by the participants will be used by the organizers for the activities of this promotion and to notify them of future campaigns of Palmolive Naturals.</p>
                            </li>

                            <li>
                                <p>User has the option to share their new hair color look unto their respective Facebook and Twitter accounts and will be prompted to log in if needed.</p>
                            </li>

                            <li>
                                <p>By supplying the above information:</p>
                                <ul>
                                    <li>Participant guarantees that she a) freely enters/participates in the promotion, and b) is not contractually prohibited by any person, company or entity from joining this promotion or from being associated with the Palmolive Naturals brand.</li>
                                    <li>Participant agrees to receive information about future campaigns of Palmolive Naturals.</li>
                                </ul>

                            </li>


                            <li>
                                <p>One submission is considered one entry. Participant can submit multiple entries but can only win once.</p>
                            </li>
                            <li>
                                <p>All submissions will be screened. Entries must not contain profanity, malicious, seditious, obscene, political and religious comments/content, or statements that are derogatory to Colgate-Palmolive, its products, other people and groups.</p>
                            </li>
                            <li>
                                <p>Only valid entries will be uploaded to the Palmolive Naturals fan page and the Palmolive Stay Vibrant microsite</p>
                            </li>
                            <li>
                                <p>Organizers reserve the right to disqualify entries in violation of the guidelines of this promotion. Organizers also have the option to take down entries that, based on their discretion, impede the promotion of the brand as well as those that present intellectual property issues. Participants warrant the original creation of their designs/entries. Participants shall indemnify and hold the organizers free and harmless from damages, injury, suit, or any cost or expense by reason of their misrepresentation of facts or warranties and of their violation of the guidelines of this promotion.</p>
                            </li>
                            <li>
                                <p>Organizers are not responsible for late, lost, illegible or incomplete entries. They do not warrant uninterrupted access to the promo by reason of, but not limited to, technical malfunction, misdirected transmissions or failures, internet access problems, etc.</p>
                            </li>


                        </ul>

                    </li>



                    <li>
                        <strong>Judging of Winners and Claiming of Prizes</strong>
                        <ol>
                            <li>
                                <p>Judging of Winners</p>
                                <ol>
                                    <li>
                                        <p>From all the valid entries, the organizers will choose five (5) winners based on the following criteria: </p>
                                        <ul>
                                            <li>Ability to show of vibrant hair and personality – 50%</li>
                                            <li>Creativity in hair color choice – 30%</li>
                                            <li>Overall appeal – 20%</li>
                                        </ul>
                                    </li>
                                    <li>
                                        <p>A representative from FDA will be invited to witness the judging of entries.</p>
                                    </li>
                                    <li>
                                        <p>The decision of the organizers is final and binding.</p>
                                    </li>
                                </ol>
                            </li>
                            <li>
                                <p>Prizes</p>
                                <ol>
                                    <li>
                                        <p>Each winner will receive the following prizes:</p>
                                        <ul>
                                            <li>Get featured in a  fashion magazine  with Janine Gutierrez</li>
                                            <li>Free hair color service (same color as their entry)</li>
                                            <li>Php 3,000 worth of Palmolive Naturals gift pack</li>
                                        </ul>
                                    </li>
                                </ol>
                            </li>
                        </ol>
                    </li>


                    <li>
                        <strong>Obligations of Winners</strong>
                        <ol>
                            <li>
                                <p>Winners must sign a consent form confirming that</p>
                                <ol>
                                    <li>
                                        <p>She agrees to have her hair dyed based on colorist’s recommendation coming from the assessment of the winner’s hair.</p>
                                        <ul>
                                            <li>i.  Organizers have the right to replace the winner with the next best entry participant in case the winner is not allowed to color her hair (based on salon's assessment on the quality of participant’s hair)</li>
                                        </ul>
                                    </li>
                                    <li>
                                        <p>She has no allergic reactions to any hair colorant ingredient.</p>
                                        <ul>
                                            <li>i.  Organizers have the right to replace the winner with the next best entry participant should the winner misrepresent or declare allergies to hair colorants.</li>
                                        </ul>

                                    </li>
                                    <li>
                                        <p>She agrees to Colgate-Palmolive’s use of her entry, photos, and behind the scenes footage and revisions / edits thereof.</p>
                                    </li>
                                </ol>
                            </li>
                            <li>
                                <p>Winners cannot be contractually prohibited to associate themselves with the Palmolive brand.</p>
                            </li>
                            <li>
                                <p>Winners will also be notified accordingly regarding the details of the rehearsal and fashion shoot.  Winners must signify their confirmation and availability to attend the said activity within seven days from their official receipt of the letter-notification. Organizers reserve the right to replace winner with the next best entry should the winner not be able to participate in the fashion shoot.</p>
                            </li>
                            <li>
                                <p>The winner may be asked to voluntarily participate in Palmolive Naturals activities based on mutually agreeable term / schedule.</p>
                            </li>

                        </ol>
                    </li>
                    <li>
                        <strong>Key Dates to Remember</strong>
                        <ol>
                            <li>
                                <p>January 17, 2016 – April 16, 2016 – Submission of entries</p>
                            </li>
                            <li>
                                <p>April 17 – 24, 2016 – Judging of winners</p>
                            </li>
                            <li>
                                <p>April 25, 2016 – Announcement of winners</p>
                            </li>

                        </ol>
                    </li>


                    <li>
                        <strong>Others</strong>
                        <ol>
                            <li>
                                <p>Organizers will contact the shortlisted participants by phone and email for verification of identity. Shortlisted participants must present valid proof of identity.</p>
                            </li>
                            <li>
                                <p>The winners will be announced on the Palmolive Naturals Facebook fan page and notified by registered mail, phone, and by e-mail.</p>
                            </li>
                            <li>
                                <p>Organizers shall determine the date of the fashion shoot in consultation with the winners and Janine with best efforts to consider their availability. If despite best efforts, there is no availability of a winner/s, the organizers have the right to replace the winner with the next best entry.</p>
                            </li>
                            <li>
                                <p>Colgate-Palmolive will not be liable for any cost, medical emergencies and/or injuries that may be encountered or sustained by the participant during the making of her entries, hair coloring session, and during the shoot, or resulting from her misrepresentation of facts / information required herein.</p>
                            </li>

                            <li>
                                <p>Prizes are non-transferable and not convertible to cash.</p>
                            </li>
                            <li>
                                <p>Employees of Colgate-Palmolive, Y&R and their relatives up to the second degree of consanguinity or affinity are not qualified to join the promo.</p>
                            </li>
                            <li>
                                <p>All entries, photos and behind-the-scenes footage of the fashion shoot, edits and revisions thereof shall be owned by Colgate-Palmolive and can be used, edited, published and displayed in any and all advertising materials on Colgate-Palmolive social media sites including Facebook, TV commercial and other advertising channels/for and internal communication materials, at any time without compensation and need for winner's consent. </p>
                            </li>
                            <li>
                                <p>Taxes on prizes, if any, will be shouldered by Colgate-Palmolive.</p>
                            </li>
                            <li>
                                <p>The promotion is in no way sponsored, endorsed or administered by, or associated with, Facebook.</p>
                            </li>
                            <li>
                                <p>The participant is providing information to Colgate-Palmolive and Y&R and not to Facebook.</p>
                            </li>
                            <li>
                                <p>Participant fully releases Facebook for any and all liabilities and injuries that may result from her participation in this promotion.</p>
                            </li>

                        </ol>
                    </li>

                </ol>
                <div class="row">
                    <div class="col-md-12" style="text-align:center !important">
                        <p>Per DOH-FDA-CCRR Permit No. 1009 s. 2015. Promo runs from January 17, 2016 to April 16, 2016</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2 col-md-offset-5 col-sm-4 col-sm-offset-4 col-xs-4 col-xs-offset-4">
                        <a href="<?php echo base_url(); ?>join/" class="btn-red large">Join</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of Main Content-->