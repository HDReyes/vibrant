    <!--Main Content-->

    <style>
        #sketchpad, #scale_btns, #rotate_btns
        {
            display: none
        }
        .croppie-container
        {
            padding: 0 !important;
        }
    </style>

    <div class="container-fluid main-content upload">
        <form action="">
        <div class="container">
            <div class="row">
                <div style="z-index:100" class="col-md-4 col-sm-8 col-sm-offset-0 col-xs-10 col-xs-offset-1 leftColumn">
                    <div class="row">
                        <div id="sketch-cont" class="col-md-12 userImage effect">
                            <!--<p class="col-md-5 tip">Brush area to color hair</p>-->
                            <div id="demo-basic"></div>

                            <img id="placeholder-img" src="<?php echo base_url(); ?>assets/images/img-user-placeholder.jpg" alt="" />
                            
                        </div>
                    </div>
                    <div class="row tools" id="rotate_btns">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6 paddingL-0">
                                    <input type="button" class="col-md-12 col-sm-12 col-xs-12 pull-right btn-red red" id="rotate-btn" value="Rotate">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 paddingL-0">
                                    <input type="button" class="col-md-12 col-sm-12 col-xs-12 pull-right btn-red red" id="rotate-save-btn" value="Save">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row tools" id="scale_btns">
                        <div class="col-md-12">
                            <div class="row">
                                <input type="button" class="col-md-4 col-sm-5 col-xs-12 pull-right btn-red red" id="crop-btn" value="Result">
                            </div>
                        </div>
                    </div>
                    <div class="row tools" id="upload_btns">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6 paddingL-0">
                                    <input id="upload-button" type="file" class="upload-photo">
                                    <label id="upload-lbl" class="col-md-12 col-sm-12 col-xs-12 btn-red upload">Upload Photo</label>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 paddingR-0">
                                    <button class="col-md-12 col-sm-12 col-xs-12 btn-red photo">Take a Photo</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-md-offset-1 col-sm-4 rightColumn">
                    <div class="col-md-12">
                        <div class="row">
                            <button type="button" class="col-md-2 btn-red clear marginR-10">Clear</button>
                            <button type="button" class="col-md-2 btn-red erase">Erase</button>
                        </div>
                    </div>
                    <br clear="all">
                    <div class="row hairColors scrollable" id="scroll-it-color">
                        <div class="col-md-12">
                            <?php $p = 0.3;?>
                            <div class="row">
                                <ul class="col-md-12" style="margin-left: 0;">
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="11,7,4,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_jet-black.jpg" alt="Jet Black">
                                            <strong>Jet Black</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="10,6,5,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_natural-black.jpg" alt="Natural Black">
                                            <strong>Natural Black</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="60,40,33,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_dark-brown.jpg" alt="Dark Brown">
                                            <strong>Dark Brown</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="165,108,65,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_platinum-blonde.jpg" alt="Platinum Blonde">
                                            <strong>Platinum Blonde</strong>
                                        </a>
                                    </li>
                                    
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="0,95,81,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_mystic-torquiose.jpg" alt="Mystic Torquiose">
                                            <strong>Mystic Torquiose</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="154,40,66,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_plum.jpg" alt="Plum">
                                            <strong>Plum</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="37,81,146,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_blue-steel.jpg" alt="Blue Steel">
                                            <strong>Blue Steel</strong>
                                        </a>
                                    </li>
                                    <li  class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="194,1,6,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_ruby-red.jpg" alt="Ruby Red">
                                            <strong>Ruby Red</strong>
                                        </a>
                                    </li>
                                    
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="204,167,149,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_rosa-rosa.jpg" alt="Rosa Rosa">
                                            <strong>Rosa Rosa</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="91,4,145,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_purple-passion.jpg" alt="Purple Passion">
                                            <strong>Purple Passion</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="148,15,98,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_ultra-violet.jpg" alt="Ultra Violet">
                                            <strong>Ultra Violet</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="195,178,158,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_vanilla-creme-blonde.jpg" alt="Vanilla Creme Blonde">
                                            <strong>Vanilla Creme Blonde</strong>
                                        </a>
                                    </li>
                                
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="205,30,59,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_burgundy-bistro.jpg" alt="Burgundy Bistro">
                                            <strong>Burgundy Bistro</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="90,49,29,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_chocolate-brown.jpg" alt="Chocolate Brown">
                                            <strong>Chocolate Brown</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="158,105,55,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_golden-brown.jpg" alt="Golden Brown">
                                            <strong>Golden Brown</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="50,117,2,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_green-grape.jpg" alt="Green Grape">
                                            <strong>Green Grape</strong>
                                        </a>
                                    </li>
                                    
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="106,65,33,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_light-brown.jpg" alt="Light Brown">
                                            <strong>Light Brown</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="132,74,36,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_medium-ash-brown.jpg" alt="Medium Ash Brown">
                                            <strong>Medium Ash Brown</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="254,72,113,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_pink-pearl.jpg" alt="Pink Pearl">
                                            <strong>Pink Pearl</strong>
                                        </a>
                                    </li>
                                    <li class="col-md-3 col-sm-6 col-xs-6">
                                        <a href="javascript:void(0)" data-olcolor="130,36,28,<?php echo $p;?>" class="outline_colors" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/img-hColor_dark-red-copper.jpg" alt="Dark Red Copper">
                                            <strong>Dark Red Copper</strong>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div><!--end:hairColors-->
                </div>
            </div>
            <div class="col-md-3 col-md-offset-7 col-sm-4 col-sm-offset-8 col-sm-12">
                <input type="button" id="save_btn" value="Submit" class="col-xs-12 btn-red large">
            </div>
        </div>
        </form>
    </div>
    <!--End of Main Content-->