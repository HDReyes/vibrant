<!-- Main Content -->
<div class="container-fluid main-content gallery">
    <div class="container">
        <h1>Gallery</h1>
        <div class="gallery-list scrollable" id="scroll-it-gallery">

            <ul>
                <li class="col-md-4 col-sm-6 col-xs-6">
                    <div class="col-md-12 text-center userImage2 effect">
                        <img src="<?php echo base_url(); ?>entries/img-user01.jpg" alt="">
                        <strong class="col-md-12 userName">Joanna Dela Cruz</strong>
                    </div>
                </li>
                <li class="col-md-4 col-sm-6 col-xs-6">
                    <div class="col-md-12 text-center userImage2 effect">
                        <img src="<?php echo base_url(); ?>entries/img-user02.jpg" alt="">
                        <strong class="col-md-12 userName">Joanna Dela Cruz</strong>
                    </div>
                </li>
                <li class="col-md-4 col-sm-6 col-xs-6">
                    <div class="col-md-12 text-center userImage2 effect">
                        <img src="<?php echo base_url(); ?>entries/img-user03.jpg" alt="">
                        <strong class="col-md-12 userName">Joanna Dela Cruz</strong>
                    </div>
                </li>
                <li class="col-md-4 col-sm-6 col-xs-6">
                    <div class="col-md-12 text-center userImage2 effect">
                        <img src="<?php echo base_url(); ?>entries/img-user04.jpg" alt="">
                        <strong class="col-md-12 userName">Joanna Dela Cruz</strong>
                    </div>
                </li>
                <li class="col-md-4 col-sm-6 col-xs-6">
                    <div class="col-md-12 text-center userImage2 effect">
                        <img src="<?php echo base_url(); ?>entries/img-user05.jpg" alt="">
                        <strong class="col-md-12 userName">Joanna Dela Cruz</strong>
                    </div>
                </li>
                <li class="col-md-4 col-sm-6 col-xs-6">
                    <div class="col-md-12 text-center userImage2 effect">
                        <img src="<?php echo base_url(); ?>entries/img-user06.jpg" alt="">
                        <strong class="col-md-12 userName">Joanna Dela Cruz</strong>
                    </div>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-2 col-md-offset-5 col-sm-4 col-sm-offset-4 col-xs-4 col-xs-offset-4 loader" style="text-align:center"></div>
        </div>
        <div class="row">
            <div class="col-md-2 col-md-offset-5 col-sm-4 col-sm-offset-4 col-xs-4 col-xs-offset-4">
                <a href="<?php echo base_url(); ?>join" class="btn-red large">Join</a>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="gallery_count" name="gallery_count" value="0">
<!-- End of Main Content -->
