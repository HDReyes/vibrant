      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
          Saitama
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2015 <a href="#">One Punch Man</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane active" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading">Tasks Progress</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <h4 class="control-sidebar-subheading">
                    Custom Template Design
                    <span class="label label-danger pull-right">70%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->

          </div><!-- /.tab-pane -->
          <!-- Stats tab content -->
          <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
          <!-- Settings tab content -->
          <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
              <h3 class="control-sidebar-heading">General Settings</h3>
              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Report panel usage
                  <input type="checkbox" class="pull-right" checked>
                </label>
                <p>
                  Some information about this general settings option
                </p>
              </div><!-- /.form-group -->
            </form>
          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>dashboard-assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url(); ?>dashboard-assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url(); ?>dashboard-assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>dashboard-assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>dashboard-assets/dist/js/app.min.js"></script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->
    <script type="text/javascript">

        (function($){

            $(window).load(function(){
                // fetch_unique_users();
                // fetch_entries();

                if ($(".unique_users").length) {
                    fetch_unique_users();
                }            

                if ($(".entries_total").length) {
                    fetch_entries();
                }            

                if ($("#example1").length) {
                    unique_dataTable_content();
                }            

                // $( ".publish" ).click(function() {
                //     var id = $(this).attr('rel');
                //     publish_action(id);
                // });
                $('body').on('click', '.publish', function () {
                    var id = $(this).attr('rel');
                    publish_action(id);
                });


                $('body').on('click', '.unpublish', function () {
                    var id = $(this).attr('rel');
                    unpublish_action(id);
                });

            });


        })(jQuery);    

        function fetch_unique_users()
        {
           $.ajax({
              method: "POST",
              url: "<?php echo base_url(); ?>dash/entry/no_unique_users/",
              data: { ajax: 1 },
              success: function(data)
              {
                    console.log(data);
                    if (data['status']) {
                        $(".unique_users").html(data['no_unique_users']);
                    }else{
                        $(".unique_users").html('nan');
                    }
              },error: function(e){
                    console.log(e);
              }                                                            
            });            
        }

        function fetch_entries()
        {
           $.ajax({
              method: "POST",
              url: "<?php echo base_url(); ?>dash/entry/all_entry/",
              data: { ajax: 1 },
              success: function(data)
              {
                    console.log(data);
                    if (data['status']) {
                        $(".entries_total").html(data['all_entries']);
                    }else{
                        $(".entries_total").html('nan');
                    }
              },error: function(e){
                    console.log(e);
              }                                                            
            });            
        }

        function publish_action(id)
        {
           $.ajax({
              method: "POST",
              url: "<?php echo base_url(); ?>dash/entry/ajax_publish/",
              data: { ajax: 1, uid: id },
              success: function(data)
              {
                    console.log(data);
                    if (data) { 
                      location.reload();
                    };
              },error: function(e){
                    console.log(e);
              }                                                            
            });  

        }

        function unpublish_action(id)
        {
           $.ajax({
              method: "POST",
              url: "<?php echo base_url(); ?>dash/entry/ajax_unpublish/",
              data: { ajax: 1, uid: id },
              success: function(data)
              {
                    console.log(data);
                    if (data) { 
                      location.reload();
                    };                    
              },error: function(e){
                    console.log(e);
              }                                                            
            });  

        }

        function unique_dataTable_content()
        {
          var table = $("#example1").DataTable();

          var data = [["Tiger Nixon","System Architect","Edinburgh","5421","2011/04/25"]];
          table.data = data;
          table.draw();
          //   $("#example1").DataTable({ 
          //     //data:data
          //     "ajax": "<?php echo base_url(); ?>dash/entry/users_with_entries/",
          //   });
           //table.data
            // $('#example2').DataTable({
            //   "paging": true,
            //   "lengthChange": false,
            //   "searching": false,
            //   "ordering": true,
            //   "info": true,
            //   "autoWidth": true
            // });  
        }

    </script>

  </body>
</html>