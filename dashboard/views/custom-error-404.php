<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
    <title></title>
</head>
<body>


    <!--Strip-->
    <div class="container-fluid strip">
        <div class="container">
            <h3 class="col-md-12">This is not the page you are looking for...</h3>
            <h6 class="col-md-8 col-md-offset-2">If that Jedi mind trick didn't worked out, you can always go back by clicking <h4><a href="<?php echo base_url(); ?>" style="text-decoration:underline">here!</a></h4></h6>
        </div>
    </div>
</body>
</html>