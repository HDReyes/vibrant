      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            All Entries
            <small>This page contains user information of ALL who joined.</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">All Entries</li>
          </ol>
        </section>


        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Entries</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Contact No.</th>
                        <th>Date/Time</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <?php 
                      if ($table_data) {
                        //echo "<pre>";
                        //print_r($table_data);
                    ?>
                        <tbody>
                          <?php foreach ($table_data as $key => $value) { ?>
                              <tr>
                                <td><?php echo $value['name']; ?></td>
                                <td><?php echo $value['email']; ?></td>
                                <td><?php echo $value['mobile']; ?></td>
                                <td><?php echo $value['entry_date']; ?></td>
                                <td align="center">
                                    <button class='btn btn-default btn-xs' data-toggle="modal" data-target="#preview<?php echo $value['entry_id']; ?>"><i class='fa fa-file-image-o'></i> View</button>

                                    <?php 
                                        if ($value['entry_status'] == 1) {
                                    ?>
                                          <button class='btn btn-default btn-xs' data-toggle="modal" data-target="#unpublish<?php echo $value['entry_id']; ?>"><i class='fa fa-thumbs-down'></i> Unpublish</button>
                                    <?php      
                                        }else{
                                    ?>
                                          <button class='btn btn-default btn-xs' data-toggle="modal" data-target="#approve<?php echo $value['entry_id']; ?>"><i class='fa fa-thumbs-up'></i> Publish</button>
                                    <?php      
                                        }
                                    ?>                                    


                                    <div class="modal fade" tabindex="-1" id="preview<?php echo $value['entry_id']; ?>" role="dialog">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title"><?php echo $value['name']; ?></h4>
                                          </div>
                                          <div class="modal-body">
                                            <p style="text-align:center"><img src="<?php echo base_url(); ?>/entries/<?php echo $value['image_name']; ?>"></p>
                                            <p><?php echo base_url(); ?>/entries/<?php echo $value['image_name']; ?></p>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          </div>
                                        </div><!-- /.modal-content -->
                                      </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->

                                    <div class="modal fade modal-success" tabindex="-1" id="approve<?php echo $value['entry_id']; ?>">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Publish <?php echo $value['name']; ?></h4>
                                          </div>
                                          <div class="modal-body">
                                            <p>Are you sure you want to PUBLISH this entry?</p>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
                                            <button type="button" class="btn btn-outline publish" data-dismiss="modal" rel="<?php echo $value['entry_id']; ?>" >Proceed</button>
                                          </div>
                                        </div><!-- /.modal-content -->
                                      </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->

                                    <div class="modal fade modal-danger" tabindex="-1" id="unpublish<?php echo $value['entry_id']; ?>">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Unpublish <?php echo $value['name']; ?></h4>
                                          </div>
                                          <div class="modal-body">
                                            <p>Are you sure you want to UNPUBLISH this entry?</p>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
                                            <button type="button" class="btn btn-outline unpublish" data-dismiss="modal" rel="<?php echo $value['entry_id']; ?>">Proceed</button>
                                          </div>
                                        </div><!-- /.modal-content -->
                                      </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->


                                </td>
                              </tr>


                          <?php } ?>
                        </tbody>                    
                    <?php      
                      }
                    ?>
                    <tfoot>
                      <tr>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Contact No.</th>
                        <th>Date/Time</th>
                        <th>Actions</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->

      </div><!-- /.content-wrapper -->