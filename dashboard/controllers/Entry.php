<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entry extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
 	public function __construct()
   	{
        parent::__construct();
        if (!$this->session->userdata('logged_in')) {
        	redirect(base_url().'dash/login/');
        }
    }	
    
	public function index()
	{
		$data['page'] = 'unique';
		$this->load->view('template/template', $data);
	}
	
	public function unique_users()
	{
		$return = $this->dashboard_model->fetch_users_with_entries();
		
		$data['table_data'] = $return['table_data'];

		$data['page'] = 'unique';
		$this->load->view('template/template', $data);
	}

	public function all_entries()
	{
		$return = $this->dashboard_model->fetch_non_unique_entries();
		
		$data['table_data'] = $return['all_entries'];

		$data['page'] = 'all-entries';
		$this->load->view('template/template', $data);
	}

	public function users_with_entries()
	{
		echo "<pre>";
		//header('Content-Type: application/json');		
		//$is_ajax = $this->input->post('ajax');
		//if ($is_ajax == 1) {
			$data = $this->dashboard_model->fetch_users_with_entries();
			//echo  count($data['table_data']);
			$var = array();
			for ($i=0; $i < count($data['table_data']); $i++) { 
				$var[] = $data['table_data'][$i]['name'];
			}

			//$g['data'] = $var[0];
			$n['data'] = [["h", "h", "h", "h", "h"]];
			$k = json_encode($n);
			print_r($data['table_data']);

		//}



	}

	// AJAX CALLS
	public function no_unique_users()
	{
		header('Content-Type: application/json');		
		$is_ajax = $this->input->post('ajax');
		if ($is_ajax == 1) {
			echo $this->dashboard_model->fetch_no_unique_users();
		}
	}

	public function all_entry()
	{
		header('Content-Type: application/json');		
		$is_ajax = $this->input->post('ajax');
		if ($is_ajax == 1) {
			echo $this->dashboard_model->fetch_all_entries();
		}

	}

	public function ajax_publish()
	{
		header('Content-Type: application/json');		
		$is_ajax = $this->input->post('ajax');
		if ($is_ajax == 1) {
			if ($this->input->post('uid')) {
				echo $this->dashboard_model->publish_account();
			}
		}

	}

	public function ajax_unpublish()
	{
		header('Content-Type: application/json');		
		$is_ajax = $this->input->post('ajax');
		if ($is_ajax == 1) {
			if ($this->input->post('uid')) {
				echo $this->dashboard_model->unpublish_account();
			}
		}

	}
}
