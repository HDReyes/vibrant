<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
 	public function __construct()
   	{
        parent::__construct();
        if (!$this->session->userdata('logged_in')) {
        	redirect(base_url().'dash/login/');
        }
    }	

	public function index()
	{
		$past = strtotime("-3 hours");
		$datetime = date("Y-m-d h:i:s", $past);
		$return = $this->dashboard_model->fetch_past_three_hours($datetime);
		$data['table_data'] = $return['all_entries'];

		$data['page'] = 'home';
		$this->load->view('template/template', $data);
	}




}
