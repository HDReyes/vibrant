<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

 	public function __construct()
   	{
        parent::__construct();
        if ($this->session->userdata('logged_in')) {
        	redirect(base_url().'dash/home/');
        }
    }	

	public function index($response = "")
	{
		$data['response'] = $response;
		$this->load->view('login', $data);
	}

	public function validate()
	{
		$this->form_validation->set_rules('username', 'username', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required');

		if ($this->form_validation->run() == FALSE)
		{			
			$this->index('Please check your username/password!');
		}else{
			$response = $this->login_model->validate_account();
			//print_r($response);
			if ($response['status']) {
				redirect(base_url().'dash/home/');
			}else{
				$this->index('Please check your username/password!');
			}

		}		
	}

	public function destroy()
	{
		$this->session->sess_destroy();
		print_r($this->session->all_userdata());
		//redirect(base_url().'dash/login/');
	}
}
