<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Update

class Dashboard_model extends CI_Model {

	 public function gallery_data($from = "")
	 {
 		$range = 0;
 		if ($from > 0) {
 			$range = $from;
 		}
	 	$return['status'] 			= "";
	 	$return['gallery_data'] 	= "";
		$sql = "SELECT users.name, users.user_id, entry.image_name, entry.date_submitted, entry.entry_id FROM tbl_entry as entry, tbl_users as users WHERE users.user_id = entry.user_id ORDER BY entry.date_submitted, RAND() LIMIT ".$range.", 10";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
		 	$return['status'] 			= TRUE;
		 	$return['gallery_data'] 	= $query->result();

		}else{
		 	$return['status'] 			= FALSE;
		}

		return $return;
	 }

	 public function fetch_no_unique_users()
	 {
	 	$return['status'] 			= "";
	 	$return['no_unique_users'] 	= 0;
		$sql = "SELECT name FROM tbl_users";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
		 	$return['status'] 			= TRUE;
		 	$return['no_unique_users'] 	= $query->num_rows();

		}else{
		 	$return['status'] 			= FALSE;
		}

		return json_encode($return);


	 }

	 public function fetch_all_entries()
	 {
	 	$return['status'] 		= "";
	 	$return['all_entries'] 	= 0;
		$sql = "SELECT entry_id FROM tbl_entry";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
		 	$return['status'] 		= TRUE;
		 	$return['all_entries'] 	= $query->num_rows();

		}else{
		 	$return['status'] 			= FALSE;
		}

		return json_encode($return);


	 }


	 public function fetch_users_with_entries()
	 {

	 	$return['status'] 		= "";
	 	$return['table_data'] 	= "";
		$sql = "SELECT  a.*, b.date_submitted, (SELECT Count(i.user_id) FROM tbl_entry i WHERE i.user_id = a.user_id) as counttotal FROM tbl_users a, tbl_entry b GROUP BY a.user_id ORDER BY b.date_submitted DESC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
		 	$return['status'] 		= TRUE;
		 	$return['table_data'] 	= $query->result_array();

		}else{
		 	$return['status'] 		= FALSE;
		}

		return $return;
	 }

	 public function fetch_non_unique_entries()
	 {
	 	$return['status'] 		= "";
	 	$return['all_entries'] 	= "";
		$sql = "SELECT  a.*, b.* FROM tbl_entry as a, tbl_users as b WHERE a.user_id = b.user_id ORDER BY a.date_submitted DESC";



		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
		 	$return['status'] 		= TRUE;
		 	$return['all_entries'] 	= $query->result_array();

		}else{
		 	$return['status'] 		= FALSE;
		}

		return $return;


	 }	 

	 public function publish_account()
	 {
	 	$entry_id = $this->input->post('uid');
	 	
	 	$return['status'] 		= "";
		$sql = "UPDATE tbl_entry SET entry_status = '1' WHERE entry_id = ?";
		$query = $this->db->query($sql, array($entry_id));
		if($this->db->affected_rows() > 0)
		{
		 	$return['status'] 		= TRUE;

		}else{
		 	$return['status'] 		= FALSE;
		}

		return json_encode($return);


	 }

 	public function unpublish_account()
	 {
	 	$entry_id = $this->input->post('uid');
	 	
	 	$return['status'] 		= "";
		$sql = "UPDATE tbl_entry SET entry_status = '0' WHERE entry_id = ?";
		$query = $this->db->query($sql, array($entry_id));
		if($this->db->affected_rows() > 0)
		{
		 	$return['status'] 		= TRUE;

		}else{
		 	$return['status'] 		= FALSE;
		}

		return json_encode($return);


	 }	 

	 public function fetch_past_three_hours($datetime = "")
	 {
	 	$return['status'] 		= "";
	 	$return['all_entries'] 	= "";
		$sql = "SELECT  a.*, b.* FROM tbl_entry as a, tbl_users as b WHERE a.user_id = b.user_id AND a.date_submitted > ? AND a.entry_status = '0' ORDER BY a.date_submitted DESC";

		$query = $this->db->query($sql, $datetime);
		if($query->num_rows() > 0)
		{
		 	$return['status'] 		= TRUE;
		 	$return['all_entries'] 	= $query->result_array();

		}else{
		 	$return['status'] 		= FALSE;
		}

		return $return;


	 }	 

}
