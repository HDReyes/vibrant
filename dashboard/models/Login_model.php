<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Update

class Login_model extends CI_Model {

	 public function validate_account()
	 {
	 	$return['status'] 			= "";
	 	$uname = $this->input->post('username');
	 	$pass  = $this->input->post('password');
		$sql = "SELECT * FROM admin_user WHERE username = ? AND password = ?";
		$query = $this->db->query($sql, array($uname, $pass));
		if($query->num_rows() > 0)
		{
		 	//SET SESSION HERE
		 	$this->_set_session($query->result());
			
		 	$return['status'] 			= TRUE;

		}else{
		 	$return['status'] 			= FALSE;
		}

		return $return;
	 }

	private function _set_session($result = "")
	{
		foreach ($result as $row) {
			$userdata = array(
                   'user_id'   => $row->admin_id,
                   'username'  => $row->username,
                   'logged_in' => TRUE
               );

			$this->session->set_userdata($userdata);
		}
	}

}
