var angle = 0,
saveRotation = function()
{
    // remove rotation container
    $("#rotate-parent").remove();

    toggleTools("scale");

    // reset value of actual image
    actualImage = new Image();

    actualImage.onload = function()
    {
        initCroppingTool();
    };

    actualImage.src = actualImgCanvas.toDataURL("image/png");
},
createRotation = function()
{
    // Step 2 : rotate thumb and actual Image


    // clear existing rotate container
    $("#rotate-parent").remove();

    // create new rotate container
    $("<div />").attr({
        "id"      : "rotate-parent"
    }).css({
        "height"  : canvasHeight,
        "width"   : canvasWidth,
        "overflow": "hidden"
    }).append(
        $("<canvas />").attr({
            "id"      : "rotate-cont",
            "height"  : canvasHeight,
            "width"   : canvasWidth,
            "display" : "none"
        })
    ).prependTo("#sketch-cont");

    thumbCanvas = document.getElementById('rotate-cont');
    thumbCtx = thumbCanvas.getContext("2d");

    drawRotation(thumbCanvas, thumbImage, angle);

    drawRotation(actualImgCanvas, actualImage, angle);
},
rotateImage = function()
{
    if(angle < 270)
    {
        angle += 90;
    }else
    {
        angle = 0;
    }

    createRotation();
},
drawRotation = function(photoCanvas,image,degrees)
{
    var ctx = photoCanvas.getContext("2d");

    if(degrees == 90 || degrees == 270) {
        photoCanvas.width = image.height;
        photoCanvas.height = image.width;
    } else {
        photoCanvas.width = image.width;
        photoCanvas.height = image.height;
    }

    ctx.clearRect(0,0,photoCanvas.width,photoCanvas.height);
    if(degrees == 90 || degrees == 270) {
        ctx.translate(image.height/2,image.width/2);
    } else {
        ctx.translate(image.width/2,image.height/2);
   }
    ctx.rotate(degrees*Math.PI/180);
    ctx.drawImage(image,-image.width/2,-image.height/2);   
};



$('.rotate-btn').on('click', rotateImage);  
$(".rotate-save-btn").on('click', saveRotation);