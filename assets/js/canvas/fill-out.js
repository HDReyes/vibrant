// Variables to keep track of the mouse position and left-button status 
var mouseX,mouseY,mouseDown=0;

// Variables to keep track of the touch position
var touchX,touchY;

var lineWidth=3;
var areaSize = 10;
var lineCoords = new Array();
var shapes     = new Array();

var olcolor = "0,0,0,0.3";// black fill color alpha

var willErase = false;

var submitEnabled = false;

var eraserSize = 2;
var currEraserSize;

// clear sketchpad
$(".clear").on('click', function()
{
    $(".tip").show();

    submitEnabled = false;

    mainCtx = mainCanvas.getContext('2d');

    // clear input file
    var control = $("#upload-button");
    control.replaceWith( control = control.clone( true ) );

    if(mainCanvas)
        clearCanvas(mainCanvas,mainCtx);

    shapes = new Array();
    actualImage = new Image();
    thumbImage   = new Image();
    thumbImgCanvas  = null;
    croppedImage = new Image();

    if(basic)
        basic.croppie('destroy');

    toggleTools("upload");

    // remove canvas
    $("#rotate-parent").remove();
    $("#sketchpad").remove();

    if(webCamVideo)
        $(".remove-webcam").trigger('click');

    createCanvas();
});

$(".erase").on('click', function(){

    willErase = true;

    switch(areaSize)
    {
        case 10:
            eraserSize = 2;
            break;
        case 20:
            eraserSize = 3;
            break;
        default:
            eraserSize = 0;
            break;
    }
    
    toggleBrush();
});

$(".outline_colors").on('click', function()
{
    willErase = false;

    lineCoords = new Array();

    toggleBrush();

    olcolor = $(this).data("olcolor");      
});

$("#save-btn").on('click', function()
{
    if(submitEnabled == true)
    {
        var backCanvas = document.createElement('canvas');
        backCanvas.width = mainCanvas.width;
        backCanvas.height = mainCanvas.height;
        var backCtx = backCanvas.getContext('2d');

        backCtx.beginPath();
        backCtx.drawImage(croppedImage, 0, 0, mainCanvas.width, mainCanvas.height);
        backCtx.closePath();


        backCtx.drawImage(mainCanvas, 0, 0);

        var data_img     = backCanvas.toDataURL("image/png");


        $("#comp_image").remove();

        var inp_img = $("<input />").attr({
            'type' : 'hidden',
            'name' : 'comp_image',
            'id'   : 'comp_image',
            'value': data_img
        });

        inp_img.prependTo("#form_editor");

        $("#form_editor").submit()
    }
});

// Set-up the canvas and add our event handlers after the page has loaded
function init() {

    // Get the specific canvas element from the HTML document

    // If the browser supports the canvas tag, get the 2d drawing context for this canvas
    if (mainCanvas.getContext)
        mainCtx = mainCanvas.getContext('2d');

    // Check that we have a valid context to draw on/with before adding event handlers
    if (mainCtx) {
        // React to mouse events on the canvas, and mouseup on the entire document
        mainCanvas.addEventListener('mousedown', sketchpad_mouseDown, false);
        mainCanvas.addEventListener('mousemove', sketchpad_mouseMove, false);
        mainCanvas.addEventListener('mouseup', sketchpad_mouseUp, false);

        // React to touch events on the canvas
        mainCanvas.addEventListener('touchstart', sketchpad_touchStart, false);
        mainCanvas.addEventListener('touchmove', sketchpad_touchMove, false);
        mainCanvas.addEventListener('touchend', sketchpad_touchEnd, false);
    }
}

function drawUplImage(olimage)
{
    // If the browser supports the canvas tag, get the 2d drawing context for this canvas
    if (mainCanvas.getContext)
        mainCtx = mainCanvas.getContext('2d');

    $("#sketchpad").css({
        'background-image' : 'url(' + olimage.src + ')',
        'background-repeat': 'no-repeat',
        'background-size'   : 'cover',
        'background-position' : 'top left' 
    });
}

// Draws a dot at a specific position on the supplied canvas name
// Parameters are: A canvas context, the x position, the y position, the size of the dot
function drawDot(ctx,x,y,size) {
    // Let's use black by setting RGB values to 0, and 255 alpha (completely opaque)
    r=0; g=0; b=0; a=255;

    var c=document.getElementById("sketchpad");
    var ctx=c.getContext("2d");

    if(willErase == true)
    {
        ctx.globalCompositeOperation = "destination-out";
        size = 10;  
        //context.strokeStyle = ("rgba(255,255,255,255)"); /* or */ 

    }else
    {
        ctx.globalCompositeOperation = "source-over";
    // Select a fill style
    ctx.fillStyle = "rgba("+olcolor+")";

    }


    // Draw a filled circle
    ctx.beginPath();
    ctx.arc(x, y, areaSize, 0, Math.PI*2); 

    //ctx.closePath();
    ctx.fill();

    lineCoords.push([x,y]);
}

// draw saved outlines
function drawCustomShape(color)
{
    var r = 0; g = 0; b = 0; a = 50;
    var eraseArr = new Array();

    drawUplImage(croppedImage);



    for (var i = 0; i < shapes.length; i++)
    {
        mainCtx.moveTo(shapes[i]['coords'][0][0], shapes[i]['coords'][0][1]);

        if(shapes[i]['type'] == 'erase')
        {
            mainCtx.globalCompositeOperation = "destination-out";
            color = "rgba(0,0,0,1)";

             for(z = 0; z < shapes[i]['coords'].length; z++)
            {
                mainCtx.beginPath();
                mainCtx.arc(shapes[i]['coords'][z][0], shapes[i]['coords'][z][1], shapes[i]['bsize'], 0, Math.PI*2); 

                mainCtx.closePath();
                mainCtx.fill();
            }     
        }
        else
        {
            mainCtx.beginPath();
            mainCtx.lineCap     =  'round';
            mainCtx.lineWidth   =  shapes[i]['bsize']*2;
            mainCtx.strokeStyle = "rgba("+shapes[i]['color']+")";

            mainCtx.globalCompositeOperation = "source-over";

            var mvcoords;

            for(x = 1; x < shapes[i]['coords'].length; x++)
            {
                if(x != 1)
                {
                    mainCtx.moveTo(mvcoords[0], mvcoords[1]);
                    mainCtx.lineTo(shapes[i]['coords'][x][0], shapes[i]['coords'][x][1]);
                }

                mvcoords = shapes[i]['coords'][x];
            }


            mainCtx.stroke();  
            mainCtx.closePath();
        }         
    }

}


// Clear the canvas context using the canvas width and height
function clearCanvas(canvas,ctx) {

    mainCtx.clearRect(0, 0, mainCanvas.width, mainCanvas.height);
}

// Keep track of the mouse button being pressed and draw a dot at current location
function sketchpad_mouseDown() {
    mouseDown=1;
    drawDot(mainCtx,mouseX,mouseY,lineWidth);
}

// Keep track of the mouse button being released
function sketchpad_mouseUp() {
    mouseDown=0;

    clearCanvas(mainCanvas, mainCtx);

    canvasData = new Array();
    canvasData['coords'] = lineCoords;

    canvasData['type']   = 'color';
    canvasData['color']  = olcolor;
    canvasData['bsize']  = areaSize;

    if(willErase == true)
    {
        canvasData['type'] = 'erase';
    }
    shapes.push(canvasData);

    drawCustomShape(olcolor);

    lineCoords = new Array();
}

// Keep track of the mouse position and draw a dot if mouse button is currently pressed
function sketchpad_mouseMove(e) { 
    // Update the mouse co-ordinates when moved
    getMousePos(e);

    // Draw a dot if the mouse button is currently being pressed
    if (mouseDown==1) {
        drawDot(mainCtx,mouseX,mouseY,lineWidth);
    }
}

// Get the current mouse position relative to the top-left of the canvas
function getMousePos(e) {
    if (!e)
        var e = event;

    if (e.offsetX) {
        mouseX = e.offsetX;
        mouseY = e.offsetY;
    }
    else if (e.layerX) {
        mouseX = e.layerX;
        mouseY = e.layerY;
    }
 }

function getPosition(element) {
    var xPosition = 0;
    var yPosition = 0;
 
    while(element) {
        xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
        yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
        element = element.offsetParent;
    }
    return { x: xPosition, y: yPosition };
}


// Get the touch position relative to the top-left of the canvas
// When we get the raw values of pageX and pageY below, they take into account the scrolling on the page
// but not the position relative to our target div. We'll adjust them using "target.offsetLeft" and
// "target.offsetTop" to get the correct values in relation to the top left of the canvas.
function getTouchPos(e) {
    // if (!e)
    //     var e = event;

    if(e.touches) {
        if (e.touches.length == 1) { // Only deal with one finger
            var touch = e.touches[0]; // Get the information for finger #1
            var canvasPos = getPosition(touch.target);

            touchX=touch.clientX - canvasPos['x'];
            touchY=touch.clientY - canvasPos['y'];
        }
    }
}

// Draw something when a touch start is detected
function sketchpad_touchStart(e)
{
    e.preventDefault();

    // Update the touch co-ordinates
    getTouchPos(e);

    drawDot(mainCtx,touchX,touchY,lineWidth);

    // Prevents an additional mousedown event being triggered
}

// Draw something and prevent the default scrolling when touch movement is detected
function sketchpad_touchMove(e) {
    // Prevent a scrolling action as a result of this touchmove triggering.
    e.preventDefault();

    // Update the touch co-ordinates
    getTouchPos(e);

    // During a touchmove event, unlike a mousemove event, we don't need to check if the touch is engaged, since there will always be contact with the screen by definition.
    drawDot(mainCtx,touchX,touchY,lineWidth); 
}


function sketchpad_touchEnd(e)
{
    e.preventDefault();


    clearCanvas(mainCanvas, mainCtx);

    canvasData = new Array();
    canvasData['coords'] = lineCoords;

    canvasData['type']   = 'color';
    canvasData['color']  = olcolor;
    canvasData['bsize']  = areaSize;

    if(willErase == true)
    {
        canvasData['type'] = 'erase';
    }
    shapes.push(canvasData);

    drawCustomShape(olcolor);

    lineCoords = new Array(); 
}

function userMedia()
{
    return navigator.getUserMedia = navigator.getUserMedia   ||
                                navigator.webkitGetUserMedia ||
                                navigator.mozGetUserMedia    ||
                                navigator.msGetUserMedia     || null;
 
}

function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}

function toggleBrush()
{
    var eraser = ((eraserSize > 0) ? '-'+eraserSize : '');

    var sketch = $("#sketchpad");

    if(willErase == true)
    {
        sketch.removeClass("brush-on");
        if(currEraserSize != null)
        {
            sketch.removeClass("brush-off" + currEraserSize);
        }
        sketch.addClass("brush-off" + eraser);
        
        currEraserSize = eraser;
    }
    else
    {
        sketch.addClass("brush-on");
        sketch.removeClass("brush-off" + eraser);
    }
}