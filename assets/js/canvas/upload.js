var placeholder = $("#placeholder-img");

var mainCanvas, mainCtx;

var canvasWidth, canvasHeight;

var take_photo_tools = $(".take-photo-btns");
var upl_tools   = $(".upload-btns");
var scale_tools = $(".scale-btns");
var brush_tools = $(".brush-btns");

var actualImage = new Image(), thumbImage = new Image();

var actualImgCanvas = document.createElement("canvas");
var actualImgCtx = actualImgCanvas.getContext("2d");

var thumbImgCanvas = document.createElement("canvas");
var thumbImgCtx = thumbImgCanvas.getContext("2d");

var webCamVideo,
    videoObj = { "video": true },
    errBack = function(error) {
        console.log("Video capture error: ", error.code); 
    };

var areaSize = 10;

var toggleTools = function(result)
{
	$(".tools").hide();

    switch(result)
    {
        case "take_photo":
            take_photo_tools.show();
            break;
        case "scale":
            scale_tools.show();
            break;
        case "upload":
            upl_tools.show();
            break;
        case "brush":
            brush_tools.show();
            break;
        default:
            break;
    }
},
toggleBrushSize = function()
{
	var activeBrush = $(this);

	$(".brush-btn").removeClass('brush-active');
	activeBrush.addClass('brush-active');


	areaSize = activeBrush.data('bsize');

	if(willErase == true)
    {
        $(".erase").trigger('click');
    }
},
createCanvas = function()
{
	if(canvasHeight == null)
	{
        canvasWidth = placeholder.width();
        canvasHeight= placeholder.height();
    }

    $("#loading-screen").css({
        width : canvasWidth,
        height: canvasHeight
    });

    $("<canvas />").attr({
        "id"      : "sketchpad",
        "class"   : "brush-on",
        "height"  : canvasHeight,
        "width"   : canvasWidth
    }).hide().prependTo("#sketch-cont");;

    mainCanvas = document.getElementById('sketchpad');

    placeholder.show();
},
getChromeVersion = function()
{     
    var raw = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./);

    return raw ? parseInt(raw[2], 10) : false;
},
trigger_upload = function(options)
{
    if(options == 'webcam')
    {
        var cver = getChromeVersion();
        var getUserSupport = true;
        
        if(cver > 46)
        {
            getUserSupport = false;
        }

        if(isMobile())
        {
            triggerUpload();

            return;
        }

        checkDeviceSupport(function() 
        {
            if(hasWebcam && getUserSupport)
            {
                if(userMedia() && isMobile() == false)
                {
                    toggleTools("take_photo");

                    var videoPlaying = false;
                    var constraints = {
                        video: true,
                        audio:false
                    }; 
                
                    $("<video />").attr({
                        'width' : canvasWidth,
                        'height': canvasHeight,
                        'id'    : 'webcam-video',
                        'autoplay' : 'autoplay'
                    }).prependTo("#sketch-cont");

                    placeholder.hide();

                    $(".tip").hide();
                    $("#webcam-video").show();

                    video = document.getElementById('webcam-video');

                    var media = navigator.getUserMedia(constraints, function(stream){

                        // URL Object is different in WebKit
                        var url = window.URL || window.webkitURL;

                        // create the url and set the source of the video element
                        video.src = url ? url.createObjectURL(stream) : stream;

                        // Start the video
                        video.play();
                        videoPlaying  = true;
                        webCamVideo = stream;
                    }, function(error){
                        console.log("ERROR");
                        console.log(error);
                    });
                }else
                {
                    alert('Webcam not supported!');  
                }
            }else
            {
                 alert('Webcam not supported!');  
            }    
        });
    }else
    {
         triggerUpload();      
    }   
},
triggerUpload = function(btn){
    console.log('clicked');
    $("#upload-button").trigger('click'); 
},
onChangeHandler = function (e)
{
    e.preventDefault();
    e = e.originalEvent;

    var target = e.dataTransfer || e.target,
        file = target && target.files && target.files[0],
        options = {
            maxWidth: canvasWidth,
            canvas: true
        };
    if (!file) {
        return;
    }

    $(".tip").hide();

    $("#loading-screen").show();

    // actual image
    loadImage.parseMetaData(file, function (data)
    {
        displayImage(file, {
             maxWidth: 800,
            canvas: true
        }, "full");
    });


},
saveToImgCanvas = function()
{
	actualImage.onload = function()
	{
		// set canvas dimensions
		actualImgCanvas.width = this.width;
		actualImgCanvas.height= this.height;

		actualImgCtx.drawImage(this, 0, 0, this.width, this.height);
	};

},
displayImage = function (file, options2, imgType)
{
    file.imageType = imgType;

    currentFile = file;
    if (!loadImage(
            file,
            replaceResults,
            options2
        ))
    {
        console.log('not supported');    
    }
},
replaceResults = function (img) 
{
    var content;

    if (img.getContext)
    {
        actualImage.onload = function()
        {
            $("#loading-screen").hide();

            initCroppingTool();

            toggleTools("scale");

            placeholder.hide();
        };

        actualImage.src = (img.src || img.toDataURL());
    }
},
isMobile = function()
{
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) 
    {    
        return true;
    }

    return false;
},
removeWebcam = function()
{
    webCamVideo.getVideoTracks()[0].stop();

    $("#webcam-video").remove();
};

$(".take-photo-btn").on("click", function()
{
    if(!webCamVideo) return;

    var webcam    = $("#webcam-video");
    var ScrCanvas = document.createElement("canvas");
    var ScrCtx    = ScrCanvas.getContext('2d');
    
    var ScrImage  = new Image(); 

    var ua        = navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    var edge = ua.indexOf('Edge/');
    var trident = ua.indexOf('Trident/');

    var scrHeight = 568;

    if(msie > 0 || edge > 0 || trident > 0)
    {
    	var scrHeight = 450;
    }

    ScrCanvas.width = 720;
    ScrCanvas.height= scrHeight;

    placeholder.show();
    $("#loading-screen").show();

    // draw first then save the image
    ScrCtx.drawImage(document.getElementById("webcam-video"), 0, 0, 720, scrHeight);

    ScrImage.src = ScrCanvas.toDataURL("image/png");

    ScrImage.onload = function()
    {
        file = dataURItoBlob(ScrImage.src);

       //actual image
        loadImage.parseMetaData(file, function (data)
        {
            displayImage(file, {
                maxWidth: 800,
                canvas: true
            }, "full");
        })
    };

    removeWebcam();
});

$(".remove-webcam").on("click", function(){

    if(webCamVideo)
    {
        webCamVideo.getVideoTracks()[0].stop();
    }

    $("#webcam-video").remove();

    $(".tip").show();
    placeholder.show();

    toggleTools("upload");

});


$(window).load(function()
{
	//Step 1 : Upload or Take Photo
	$(".upload-lbl").on('click', function()
	{
		trigger_upload();
	});

	$(".webcam-lbl").on('click', function()
	{
		trigger_upload('webcam');
	});


	$('#upload-button').on('change', onChangeHandler);
	$('.brush-btn').on('click', toggleBrushSize);

	createCanvas();

	toggleTools('upload');
});




if (navigator.mediaDevices && navigator.mediaDevices.enumerateDevices) {
    // Firefox 38+ seems having support of enumerateDevicesx
    navigator.enumerateDevices = function(callback) {
        navigator.mediaDevices.enumerateDevices().then(callback);
    };
}

var MediaDevices = [];
var isHTTPs = location.protocol === 'https:';
var canEnumerate = false;

if (typeof MediaStreamTrack !== 'undefined' && 'getSources' in MediaStreamTrack) {
    canEnumerate = true;
} else if (navigator.mediaDevices && !!navigator.mediaDevices.enumerateDevices) {
    canEnumerate = true;
}

var hasMicrophone = false;
var hasSpeakers = false;
var hasWebcam = false;

var isMicrophoneAlreadyCaptured = false;
var isWebcamAlreadyCaptured = false;

function checkDeviceSupport(callback) {

    if (!canEnumerate) {
        triggerUpload();
        return;
    }

    if (!navigator.enumerateDevices && window.MediaStreamTrack && window.MediaStreamTrack.getSources) {
        navigator.enumerateDevices = window.MediaStreamTrack.getSources.bind(window.MediaStreamTrack);
    }

    if (!navigator.enumerateDevices && navigator.enumerateDevices) {
        navigator.enumerateDevices = navigator.enumerateDevices.bind(navigator);
    }

    if (!navigator.enumerateDevices) {
        if (callback) {
            callback();
        }
        return;
    }

    MediaDevices = [];
    navigator.enumerateDevices(function(devices) {
        devices.forEach(function(_device) {
            var device = {};
            for (var d in _device) {
                device[d] = _device[d];
            }

            if (device.kind === 'audio') {
                device.kind = 'audioinput';
            }

            if (device.kind === 'video') {
                device.kind = 'videoinput';
            }

            var skip;
            MediaDevices.forEach(function(d) {
                if (d.id === device.id && d.kind === device.kind) {
                    skip = true;
                }
            });

            if (skip) {
                return;
            }

            if (!device.deviceId) {
                device.deviceId = device.id;
            }

            if (!device.id) {
                device.id = device.deviceId;
            }

            if (!device.label) {
                device.label = 'Please invoke getUserMedia once.';
                if (!isHTTPs) {
                    device.label = 'HTTPs is required to get label of this ' + device.kind + ' device.';
                }
            } else {
                if (device.kind === 'videoinput' && !isWebcamAlreadyCaptured) {
                    isWebcamAlreadyCaptured = true;
                }

                if (device.kind === 'audioinput' && !isMicrophoneAlreadyCaptured) {
                    isMicrophoneAlreadyCaptured = true;
                }
            }

            if (device.kind === 'audioinput') {
                hasMicrophone = true;
            }

            if (device.kind === 'audiooutput') {
                hasSpeakers = true;
            }

            if (device.kind === 'videoinput') {
                hasWebcam = true;
            }

            // there is no 'videoouput' in the spec.

            MediaDevices.push(device);
        });

        if (callback) {
            callback();
        }
    });
}


