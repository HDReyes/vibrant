
var croppedImage = new Image();
var basic;


var croppable = false;
var crop_img;

var initCroppingTool = function()
{	
	var viewWidth = 380, viewHeight = 437;
  var crop_cont = $("#demo-basic");

	if(canvasWidth < 350)
	{
		viewWidth = 200;
		viewHeight= 230;
	}

    actualImage.id = "crop-img";

    crop_cont.css({
        'width' : canvasWidth,
        'height': canvasHeight
    });

    crop_cont.append(actualImage);

    crop_img = $("#crop-img");

    crop_img.cropper({
          viewMode: 3,
          dragMode: 'move',
          autoCropArea: 1,
          restore: false,
          guides: true,
          highlight: true,
          cropBoxMovable: false,
          cropBoxResizable: false,
          built: function () {
              croppable = true;


            }
        });



},
removeCropper = function()
{
  $("#demo-basic").remove();

  $("#sketch-cont").append($("<div />").attr('id', 'demo-basic'));
};

$('.crop-btn').on('click', function() 
{
    submitEnabled = true;

    var croppedCanvas;

    if (!croppable) {
      return;
    }

    // Crop
    croppedCanvas = crop_img.cropper('getCroppedCanvas');

    croppedImage.src = croppedCanvas.toDataURL();

    drawUplImage(croppedImage);

    init();
    
    $("#sketchpad").show();

    // hide tools
    toggleTools('brush');

    removeCropper();
});

$('.upload-config-btns').on('click', function()
{
  if(croppable)
  {
    var config_btn = $(this); 

    crop_img.cropper(config_btn.data('method'), config_btn.data('option'));
  }
});
