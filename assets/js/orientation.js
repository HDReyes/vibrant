/*
 * JavaScript Load Image Demo JS 1.9.1
 * https://github.com/blueimp/JavaScript-Load-Image
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*global window, document, loadImage, HTMLCanvasElement, $ */

var fullPhotoCanvas = document.createElement("canvas"),
    fullPhotoImage  = new Image(),
    fullPhotoCtx = fullPhotoCanvas.getContext("2d"),
    thumbCanvas,
    angle = 0,
    thumbCtx,
    currentFile;

var saveRotation = function()
{
    // remove rotation container
    $("#rotate-parent").remove();

    toggleTools("scale");

    fullPhotoImage.onload = function()
    {
        console.log(fullPhotoCanvas.toDataURL("image/png"));

        initCroppingTool(removeBlanks(fullPhotoCanvas, fullPhotoCtx, fullPhotoCanvas.width, fullPhotoCanvas.height));
    };

    

    fullPhotoImage.src = fullPhotoCanvas.toDataURL("image/png");
},
removeBlanks = function (blCanvas, blContext, imgWidth, imgHeight) {
    var imageData = blContext.getImageData(0, 0, blCanvas.width, blCanvas.height),
             data = imageData.data,
           getRBG = function(x, y) {
                      return {
                        red:   data[(imgWidth*y + x) * 4],
                        green: data[(imgWidth*y + x) * 4 + 1],
                        blue:  data[(imgWidth*y + x) * 4 + 2]
                      };
                    },
          isWhite = function (rgb) {
                      return rgb.red == 255 && rgb.green == 255 && rgb.blue == 255;
                    },
            scanY = function (fromTop) {
                      var offset = fromTop ? 1 : -1;

                      // loop through each row
                      for(var y = fromTop ? 0 : imgHeight - 1; fromTop ? (y < imgHeight) : (y > -1); y += offset) {

                        // loop through each column
                        for(var x = 0; x < imgWidth; x++) {
                            if (!isWhite(getRBG(x, y))) {
                                return y;                        
                            }      
                        }
                    }
                    return null; // all image is white
                },
            scanX = function (fromLeft) {
                      var offset = fromLeft? 1 : -1;

                      // loop through each column
                      for(var x = fromLeft ? 0 : imgWidth - 1; fromLeft ? (x < imgWidth) : (x > -1); x += offset) {

                        // loop through each row
                        for(var y = 0; y < imgHeight; y++) {
                            if (!isWhite(getRBG(x, y))) {
                                return x;                        
                            }      
                        }
                    }
                    return null; // all image is white
                };


        var cropTop = scanY(true),
            cropBottom = scanY(false),
            cropLeft = scanX(true),
            cropRight = scanX(false);
    // cropTop is the last topmost white row. Above this row all is white
    // cropBottom is the last bottommost white row. Below this row all is white
    // cropLeft is the last leftmost white column.
    // cropRight is the last rightmost white column.

    return blCanvas;
},
rotateImage = function()
{
    if(angle < 270)
    {
        angle += 90;
    }else
    {
        angle = 0;
    }

    createRotation();
},
drawRotation = function(photoCanvas,image,ctx,degrees)
{
    if(degrees == 90 || degrees == 270) {
        photoCanvas.width = image.height;
        photoCanvas.height = image.width;
    } else {
        photoCanvas.width = image.width;
        photoCanvas.height = image.height;
    }

    ctx.clearRect(0,0,photoCanvas.width,photoCanvas.height);
    if(degrees == 90 || degrees == 270) {
        ctx.translate(image.height/2,image.width/2);
    } else {
        ctx.translate(image.width/2,image.height/2);
   }
    ctx.rotate(degrees*Math.PI/180);
    ctx.drawImage(image,-image.width/2,-image.height/2);   
},
createRotation = function()
{
    $("#rotate-parent").remove();

    $("<div />").attr({
        "id"      : "rotate-parent"
    }).css({
        "height"  : canvasHeight,
        "width"   : canvasWidth,
        "overflow": "hidden"
    }).append(
        $("<canvas />").attr({
            "id"      : "rotate-cont",
            "height"  : canvasHeight,
            "width"   : canvasWidth,
            "display" : "none"
        })
    ).prependTo("#sketch-cont");

    thumbCanvas = document.getElementById('rotate-cont');
    thumbCtx = thumbCanvas.getContext("2d");

    drawRotation(thumbCanvas, thumbImage, thumbCtx, angle);
    drawRotation(fullPhotoCanvas, outlineImage, fullPhotoCtx, angle);
},
replaceResults = function (img) 
{
    console.log('test');

    var content;

    if (img.getContext)
    {
        if(img.getAttribute("type") == "thumb")
        {
            outlineImage.onload = function()
            {
                toggleTools("rotate");

                createRotation();

                placeholder.hide();
            };

            thumbImage.src = (img.src || img.toDataURL());
        }
        else
        {
            outlineImage.src = (img.src || img.toDataURL());
        }
    }
},
displayImage = function (file, options2, imgType)
{
    file.imageType = imgType;

    currentFile = file;
    if (!loadImage(
            file,
            replaceResults,
            options2
        ))
    {
        console.log('not supported');    
    }
},
onChangeHandler = function (e)
{
    e.preventDefault();
    e = e.originalEvent;

    var target = e.dataTransfer || e.target,
        file = target && target.files && target.files[0],
        options = {
            maxWidth: canvasWidth,
            canvas: true
        };
    if (!file) {
        return;
    }

    $(".tip").hide();

    // thumb image
    loadImage.parseMetaData(file, function (data)
    {
        displayImage(file, {
            maxWidth: canvasWidth,
            canvas: true
        }, "thumb");
    });


    // actual image
    loadImage.parseMetaData(file, function (data)
    {
        displayImage(file, {
            maxWidth: 400,
            maxHeight: 300,
            canvas: true
        }, "full");
    });

},
coordinates;


$(function ()
{
    'use strict';

    $('#upload-button').on('change', onChangeHandler);

    $('#rotate-btn').on('click', rotateImage);
    
    $("#rotate-save-btn").on('click', saveRotation);
});
