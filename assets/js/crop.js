var outlineImage;
var croppedImage = new Image();

var placeholder = $("#placeholder-img");

var canvasWidth, canvasHeight;

function previewURL(input)
{
    if(input.files && input.files[0])
    {
        orientChangeHandler(input);

        // var reader = new FileReader();

        // reader.onload = function(e)
        // {
        //     outlineImage.src = e.target.result;
        // }

        // reader.readAsDataURL(input.files[0]);

        // //init();

        // outlineImage.onload = function()
        // {
        //     console.log(outlineImage.src);

        //     toggleScaleTools(true);

        //     placeholder.hide();

        //     initCroppingTool();

        //     //drawUplImage(outlineImage);
        // };

        croppedImage.onload = function()
        {
            console.log('test');
            //drawUplImage(croppedImage);
        }
    }
}

function toggleScaleTools(result)
{
    var scale_tools = $("#scale_btns");
    var upl_tools   = $("#upload_btns");

    scale_tools.hide();
    upl_tools.hide();

    if(result)
    {
        scale_tools.show();
    }else
    {
        upl_tools.show();
    }
}

function upd_range(zoom)
{
    $("#scale_tool").attr('step', zoom);
}

function initCroppingTool()
{
    console.log(outlineImage);

    var basic = $('#demo-basic').croppie({
        viewport: {
            width: 200,
            height: 230
        },
        boundary: {
            width: canvasWidth, 
            height: canvasHeight
        }
    });
    basic.croppie('bind', {
        url: outlineImage,
        points: [0,0,280,739]
    });


    //on button click
    $('#crop-btn').on('click', function() 
    {
        basic.croppie('result', {
            type : 'canvas',
            size : 'viewport'
        }).then(function (resp)
        {
            console.log(resp);    
            //$("#sketchpad").show();

            //toggleScaleTools(false);

            //croppedImage.src = resp;

            //basic.croppie('destroy');
        });
    });

}

function createCanvas()
{
    if(canvasHeight == null)
    {
        canvasWidth = placeholder.width();
        canvasHeight= placeholder.height();
    }

    $("<canvas />").attr({
        "id"      : "sketchpad",
        "height"  : canvasHeight,
        "width"   : canvasWidth,
        "display" : "none"
    }).prependTo("#sketch-cont");;

    canvas = document.getElementById('sketchpad');

    placeholder.show();
}


$(window).load(function()
{
    // on click of upload photo
    $("#upload-lbl").on('click', function()
    {
        $("#upload-button").trigger('click');       
    });

    createCanvas();

});
