var outlineImage = new Image();
var thumbImage   = new Image();
// Variables for referencing the canvas and 2dcanvas context
var canvas;

var ctx;

// Variables to keep track of the mouse position and left-button status 
var mouseX,mouseY,mouseDown=0;

// Variables to keep track of the touch position
var touchX,touchY;

var lineWidth=10;

var lineCoords = new Array();
var shapes     = new Array();

var olcolor = "0,0,0,0.1";// black fill color alpha

var croppedImage = new Image();

var webCamVideo,
    videoObj = { "video": true },
    errBack = function(error) {
        console.log("Video capture error: ", error.code); 
    };

var placeholder = $("#placeholder-img");

var canvasWidth, canvasHeight;
var willErase = false;

var take_photo_tools = $("#take_photo_btns");
var rotate_tools= $("#rotate_btns");
var scale_tools = $("#scale_btns");
var upl_tools   = $("#upload_btns");
var basic;
var submitEnabled = false;

function toggleTools(result)
{
    $(".tools").hide();

    switch(result)
    {
        case "take_photo":
            take_photo_tools.show();
            break;
        case "rotate":
            rotate_tools.show();
            break;
        case "scale":
            scale_tools.show();
            break;
        case "upload":
            upl_tools.show();
            break;
        default:
            break;
    }
}

function initCroppingTool(canvasImage)
{
    basic = $('#demo-basic').croppie({
        viewport: {
            width: 200,
            height: 230
        },
        boundary: {
            width: canvasWidth, 
            height: canvasHeight
        }
    });
    basic.croppie('bind', {
        url : canvasImage.toDataURL("image/png"),
        points : [0,0, 200, -1]
    });


    

    //on button click
    $('#crop-btn').on('click', function() 
    {
        submitEnabled = true;

        basic.croppie('result', {
            type : 'canvas',
            size : 'viewport'
        }).then(function (resp)
        { 
            croppedImage.src = resp;
            drawUplImage(croppedImage);

            init();

            $("#sketchpad").show();

            toggleTools();

            basic.croppie('destroy');
        });
    });

}


// Set-up the canvas and add our event handlers after the page has loaded
function init() {

    // Get the specific canvas element from the HTML document

    // If the browser supports the canvas tag, get the 2d drawing context for this canvas
    if (canvas.getContext)
        ctx = canvas.getContext('2d');

    // Check that we have a valid context to draw on/with before adding event handlers
    if (ctx) {
        // React to mouse events on the canvas, and mouseup on the entire document
        canvas.addEventListener('mousedown', sketchpad_mouseDown, false);
        canvas.addEventListener('mousemove', sketchpad_mouseMove, false);
        canvas.addEventListener('mouseup', sketchpad_mouseUp, false);

        // React to touch events on the canvas
        canvas.addEventListener('touchstart', sketchpad_touchStart, false);
        canvas.addEventListener('touchmove', sketchpad_touchMove, false);
        canvas.addEventListener('touchend', sketchpad_touchEnd, false);
    }
}

function drawUplImage(olimage)
{
    // If the browser supports the canvas tag, get the 2d drawing context for this canvas
    if (canvas.getContext)
        ctx = canvas.getContext('2d');

    $("#sketchpad").css({
        'background-image' : 'url(' + olimage.src + ')',
        'background-repeat': 'no-repeat',
        'background-size'   : 'cover',
        'background-position' : 'top left' 
    });
}

function createCanvas()
{
    if(canvasHeight == null){
        canvasWidth = placeholder.width();
        canvasHeight= placeholder.height();
    }

    $("<canvas />").attr({
        "id"      : "sketchpad",
        "class"   : "brush-on",
        "height"  : canvasHeight,
        "width"   : canvasWidth
    }).hide().prependTo("#sketch-cont");;

    canvas = document.getElementById('sketchpad');

    placeholder.show();
}

// Draws a dot at a specific position on the supplied canvas name
// Parameters are: A canvas context, the x position, the y position, the size of the dot
function drawDot(ctx,x,y,size) {
    // Let's use black by setting RGB values to 0, and 255 alpha (completely opaque)
    r=0; g=0; b=0; a=255;

    var c=document.getElementById("sketchpad");
    var ctx=c.getContext("2d");

    if(willErase == true)
    {
        ctx.globalCompositeOperation = "destination-out";
        size = 10;  
        //context.strokeStyle = ("rgba(255,255,255,255)"); /* or */ 

    }else
    {
        ctx.globalCompositeOperation = "source-over";
    // Select a fill style
    ctx.fillStyle = "rgba("+olcolor+")";

    }


    // Draw a filled circle
    ctx.beginPath();
    ctx.arc(x, y, size, 0, Math.PI*2); 

    //ctx.closePath();
    ctx.fill();

    lineCoords.push([x,y]);
}

// draw saved outlines
function drawCustomShape(color)
{
    var r = 0; g = 0; b = 0; a = 50;

    drawUplImage(croppedImage);

    for (var i = 0; i < shapes.length; i++)
    {
        ctx.moveTo(shapes[i]['coords'][0][0], shapes[i]['coords'][0][1]);

        if(shapes[i]['type'] == 'erase')
        {
            ctx.globalCompositeOperation = "destination-out";
            color = "rgba(0,0,0,1)";

             for(x = 1; x < shapes[i]['coords'].length; x++)
            {
                ctx.beginPath();
                ctx.arc(shapes[i]['coords'][x][0], shapes[i]['coords'][x][1], 20, 0, Math.PI*2); 

                ctx.closePath();
                ctx.fill();
                
            }
        }else
        {
             ctx.beginPath();
            ctx.globalCompositeOperation = "source-over";

            for(x = 1; x < shapes[i]['coords'].length; x++)
            {
                ctx.lineTo(shapes[i]['coords'][x][0], shapes[i]['coords'][x][1]);
            }

            ctx.fillStyle = "rgba("+color+")";
            ctx.fill();  
             ctx.closePath();  
        }          

       
    }
}

// Clear the canvas context using the canvas width and height
function clearCanvas(canvas,ctx) {

    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

// Keep track of the mouse button being pressed and draw a dot at current location
function sketchpad_mouseDown() {
    mouseDown=1;
    drawDot(ctx,mouseX,mouseY,lineWidth);
}

// Keep track of the mouse button being released
function sketchpad_mouseUp() {
    mouseDown=0;

    //clearCanvas(canvas, ctx);

    canvasData = new Array();
    canvasData['coords'] = lineCoords;

    canvasData['type']   = 'color';
    if(willErase == true)
    {
        canvasData['type'] = 'erase';
    }
    shapes.push(canvasData);

    //drawCustomShape(olcolor);

    lineCoords = new Array(); 
}

// Keep track of the mouse position and draw a dot if mouse button is currently pressed
function sketchpad_mouseMove(e) { 
    // Update the mouse co-ordinates when moved
    getMousePos(e);

    // Draw a dot if the mouse button is currently being pressed
    if (mouseDown==1) {
        drawDot(ctx,mouseX,mouseY,lineWidth);
    }
}

// Get the current mouse position relative to the top-left of the canvas
function getMousePos(e) {
    if (!e)
        var e = event;

    if (e.offsetX) {
        mouseX = e.offsetX;
        mouseY = e.offsetY;
    }
    else if (e.layerX) {
        mouseX = e.layerX;
        mouseY = e.layerY;
    }
 }

function getPosition(element) {
    var xPosition = 0;
    var yPosition = 0;
 
    while(element) {
        xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
        yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
        element = element.offsetParent;
    }
    return { x: xPosition, y: yPosition };
}


// Get the touch position relative to the top-left of the canvas
// When we get the raw values of pageX and pageY below, they take into account the scrolling on the page
// but not the position relative to our target div. We'll adjust them using "target.offsetLeft" and
// "target.offsetTop" to get the correct values in relation to the top left of the canvas.
function getTouchPos(e) {
    // if (!e)
    //     var e = event;

    if(e.touches) {
        if (e.touches.length == 1) { // Only deal with one finger
            var touch = e.touches[0]; // Get the information for finger #1
            var canvasPos = getPosition(touch.target);

            touchX=touch.clientX - canvasPos['x'];
            touchY=touch.clientY - canvasPos['y'];
        }
    }
}

// Draw something when a touch start is detected
function sketchpad_touchStart(e)
{
    e.preventDefault();

    // Update the touch co-ordinates
    getTouchPos(e);

    drawDot(ctx,touchX,touchY,lineWidth);

    // Prevents an additional mousedown event being triggered
}

// Draw something and prevent the default scrolling when touch movement is detected
function sketchpad_touchMove(e) {
    // Prevent a scrolling action as a result of this touchmove triggering.
    e.preventDefault();

    // Update the touch co-ordinates
    getTouchPos(e);

    // During a touchmove event, unlike a mousemove event, we don't need to check if the touch is engaged, since there will always be contact with the screen by definition.
    drawDot(ctx,touchX,touchY,lineWidth); 
}


function sketchpad_touchEnd(e)
{
    e.preventDefault();


    //clearCanvas(canvas, ctx);

    canvasData = new Array();
    canvasData['coords'] = lineCoords;

    canvasData['type']   = 'color';
    if(willErase == true)
    {
        canvasData['type'] = 'erase';
    }
    shapes.push(canvasData);

    //drawCustomShape(olcolor);

    lineCoords = new Array(); 
}

function userMedia()
{
    return navigator.getUserMedia = navigator.getUserMedia   ||
                                navigator.webkitGetUserMedia ||
                                navigator.mozGetUserMedia    ||
                                navigator.msGetUserMedia     || null;
 
}

function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}

$("#take-photo-btn").on("click", function(){

    var webcam    = $("#webcam-video");
    var ScrCanvas = document.createElement("canvas");
    var ScrCtx    = ScrCanvas.getContext('2d');
    
    var ScrImage  = new Image(); 

    ScrCanvas.width = 720;
    ScrCanvas.height= 546;

    // draw first then save the image
    ScrCtx.drawImage(document.getElementById("webcam-video"), 0, 0, 720, 546);

    ScrImage.src = ScrCanvas.toDataURL("image/png");

    file = dataURItoBlob(ScrImage.src);

    loadImage.parseMetaData(file, function (data)
    {
        displayImage(file, {
            maxWidth: canvasWidth,
            canvas: true
        }, "thumb");
    });

    // actual image
    loadImage.parseMetaData(file, function (data)
    {
        displayImage(file, {
            maxWidth: 400,
            maxHeight: 300,
            canvas: true
        }, "full");
    })


    webCamVideo.getVideoTracks()[0].stop();

    $("#webcam-video").remove();
});

$("#remove-webcam").on("click", function(){

    webCamVideo.getVideoTracks()[0].stop();

    $("#webcam-video").remove();

    placeholder.show();

    toggleTools("upload");

});

function isMobile()
{
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) 
    {    
        return true;
    }

    return false;
}

function trigger_upload(options)
{
    var will_upload = true;

    if(options == 'webcam')
    {
        if(userMedia() && isMobile() == false)
        {
            toggleTools("take_photo");

            var videoPlaying = false;
            var constraints = {
                video: true,
                audio:false
            }; 
        
            $("<video />").attr({
                'width' : canvasWidth,
                'height': canvasHeight,
                'id'    : 'webcam-video',
                'autoplay' : 'autoplay'
            }).prependTo("#sketch-cont");

            placeholder.hide();
            $("#webcam-video").show();

            video = document.getElementById('webcam-video');

            var media = navigator.getUserMedia(constraints, function(stream){

                // URL Object is different in WebKit
                var url = window.URL || window.webkitURL;

                // create the url and set the source of the video element
                video.src = url ? url.createObjectURL(stream) : stream;

                // Start the video
                video.play();
                videoPlaying  = true;
                webCamVideo = stream;
            }, function(error){
                console.log("ERROR");
                console.log(error);
            });

            will_upload = false;
        }
    }

    if(will_upload == true)
        $("#upload-button").trigger('click');    

}

$(window).load(function()
{
	// on click of upload photo
	$("#upload-lbl").on("click", function(){
        trigger_upload();
    });

    $("#webcam-lbl").on("click", function(){
        trigger_upload("webcam");
    });

	// clear sketchpad
    $(".clear").on('click', function()
    {
        submitEnabled = false;

    	ctx = canvas.getContext('2d');

        // clear input file
        var control = $("#upload-button");
        control.replaceWith( control = control.clone( true ) );

        if(canvas)
            clearCanvas(canvas,ctx);

        shapes = new Array();
        outlineImage = new Image();
        thumbImage   = new Image();
        thumbCanvas  = null;
        croppedImage = new Image();

        toggleTools("upload");

        // remove canvas
        $("#rotate-parent").remove();
        $("#sketchpad").remove();

        createCanvas();
    });
    
    $(".erase").on('click', function(){

        willErase = true;

        toggleBrush();
    });

    function toggleBrush()
    {
        $("#sketchpad").toggleClass("brush-on");
        $("#sketchpad").toggleClass("brush-off");
    }

    $("#save_btn").on('click', function()
    {
        if(submitEnabled == true)
        {
            var backCanvas = document.createElement('canvas');
            backCanvas.width = canvas.width;
            backCanvas.height = canvas.height;
            var backCtx = backCanvas.getContext('2d');

            backCtx.beginPath();
            backCtx.drawImage(croppedImage, 0, 0, canvas.width, canvas.height);
            backCtx.closePath();


            backCtx.drawImage(canvas, 0, 0);

            var data_img     = backCanvas.toDataURL("image/png");


            $("#comp_image").remove();

            var inp_img = $("<input />").attr({
                'type' : 'hidden',
                'name' : 'comp_image',
                'id'   : 'comp_image',
                'value': data_img
            });

            inp_img.prependTo("#form_editor");

            $("#form_editor").submit()
        }
    });

    $(".outline_colors").on('click', function()
    {
        toggleBrush();

        willErase = false;

        olcolor = $(this).data("olcolor");

        //clearCanvas(canvas, ctx);

        //drawCustomShape(olcolor);        
    });

    createCanvas();

    toggleTools("upload");
});