$(window).load(function(){


				$("#scroll-it-gallery-main").mCustomScrollbar({
					theme:"rounded",
                    advanced:{ updateOnContentResize: true },
                    advanced:{ updateOnImageLoad: true },
                    callbacks:{
                        onTotalScroll:function(){
                            console.log("Scrolled to end of content.");
                            var gal_count = $('#gallery_count').val();

                            var loaderContainer = $('.loader');
                            var img = jQuery("<img class='imageLoader' src='" + base_url + "assets/images/7152.GIF' style='align:center; margin:0px 0px 10px 0px; height:30px; width:30px;'>");                      
                            loaderContainer.prepend(img);

                           $.ajax({
                              method: "POST",
                              url: base_url + "gallery/fetch_gallery/",
                              data: { from: gal_count, ajax: 1 },
                              success: function(data)
                              {
                                    console.log(data);
                                    setTimeout(function() {
                                      //$('.message').remove();
                                      img.remove();
                                    }, 2000);

                                    if (data['status']) {
                                        
                                            // Sums up the total row up this point 
                                            $('#gallery_count').val(Number(data['gallery_data'].length) + Number(gal_count));
                                            for (var i = 0; i < data['gallery_data'].length; i++) {
                                                var name  = data['gallery_data'][i]['name'].split(" ");
                                                var fname = name[0];
                                                var lname = name[1];                                                

                                                  var appendOpen = "<li class='col-md-4 col-sm-6 col-xs-6'>";
                                                  var appendBody = "<a class='col-md-12 text-center userImage2 effect' target='_blank' href='" + base_url + "gallery/page?u="+data['gallery_data'][i]['entry_id']+"'>";
                                                      appendBody = appendBody + "<img src='" + base_url + "entries/" + data['gallery_data'][i]['image_name'] + "' alt=''>";
                                                      appendBody = appendBody + "<strong class='col-md-12 userName'>" + fname + "</strong>";
                                                      appendBody = appendBody + "</a>";
                                                  var appnedClose = "</li>";
                                                  $("#scroll-it-gallery-main ul").append(appendOpen + appendBody + appnedClose);                                        
                                            };                                    

                                    }else{

                                      var msg = "<div class='row text-center'><h5 class='col-xs-12 paddingL-0'><p>There are no entry at the moment, be the first the join!</p></h5></div>";
                                      $("#scroll-it-gallery-main ul").prepend(msg);

                                    }
                              },error: function(e){
                                    //console.log(e);
                              }                                                            
                            });



                        }
                    }
				});

        $("#scroll-it-gallery-sub").mCustomScrollbar({
          theme:"rounded",
                    advanced:{ updateOnContentResize: true },
                    advanced:{ updateOnImageLoad: true },
                    callbacks:{
                        onTotalScroll:function(){
                            var gal_count = $('#gallery_count').val();

                            var loaderContainer = $('.loader');
                            var img = jQuery("<img class='imageLoader' src='" + base_url + "assets/images/7152.GIF' style='align:center; margin:0px 0px 10px 0px; height:30px; width:30px;'>");                      
                            loaderContainer.prepend(img);

                           $.ajax({
                              method: "POST",
                              url: base_url + "gallery/fetch_gallery/",
                              data: { from: gal_count, ajax: 1 },
                              success: function(data)
                              {
                                    console.log(data);
                                    setTimeout(function() {
                                      //$('.message').remove();
                                      img.remove();
                                    }, 2000);

                                    if (data['status']) {
                                            // Sums up the total row up this point 
                                            $('#gallery_count').val(Number(data['gallery_data'].length) + Number(gal_count));
                                            for (var i = 0; i < data['gallery_data'].length; i++) {
                                                var name  = data['gallery_data'][i]['name'].split(" ");
                                                var fname = name[0];
                                                var lname = name[1];    

                                                  var appendOpen = "<li class='col-xs-4'>";
                                                  var appendBody = "<a class='row text-center userImage2 effect' data-toggle='modal' data-target='#" + data['gallery_data'][i]['entry_id'] + "' href='#' onclick='return false'>";
                                                      appendBody = appendBody + "<img src='" + base_url + "entries/" + data['gallery_data'][i]['image_name'] + "' alt=''>";
                                                      appendBody = appendBody + "<strong class='col-md-12 userName'>" + fname + "</strong>";
                                                      appendBody = appendBody + "</a>";
                                                      appendBody = appendBody + "<div id='" + data['gallery_data'][i]['entry_id'] + "' class='modal fade' role='dialog' aria-hidden='true'>";
                                                      appendBody = appendBody + "<div class='modal-dialog'>";
                                                      appendBody = appendBody + "<div class='modal-content col-xs-8 col-xs-offset-2'>";
                                                      appendBody = appendBody + "<div class='modal-header row'>";
                                                      appendBody = appendBody + "<h5 class='col-xs-8 paddingL-0'>" + data['gallery_data'][i]['name'] + "<h5>";
                                                      appendBody = appendBody + "<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>";
                                                      appendBody = appendBody + "</div>";
                                                      appendBody = appendBody + "<div class='modal-body row'><img src='" + base_url + "entries/" + data['gallery_data'][i]['image_name'] + "' alt='' class=''></div>";
                                                      // appendBody = appendBody + "<a href='#/' class='facebook_share_profile' rel='"+data['gallery_data'][i]['name']+"'>facebook</a>";
                                                      // appendBody = appendBody + "<a href='#/' class='twitter_share_profile' rel='"+data['gallery_data'][i]['entry_id']+"'>twitter</a>";
                                                       appendBody = appendBody + "<div class='row'>";
                                                             appendBody = appendBody + "<div class='col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 text-center share'>";
                                                                 appendBody = appendBody + "<div class='row'>";
                                                                     appendBody = appendBody + "<div class='col-md-6 col-sm-6 col-xs-6'>";
                                                                         appendBody = appendBody + "<a href='#/' data-eid='" + data['gallery_data'][i]['entry_id'] + "' class='facebook-pink pull-right facebook_share_profile' rel='"+data['gallery_data'][i]['image_name']+"'></a>";
                                                                     appendBody = appendBody + "</div>";
                                                                     appendBody = appendBody + "<div class='col-md-4 col-sm-4 col-xs-4'>";
                                                                         appendBody = appendBody + "<a href='#/' class='twitter-pink pull-left twitter_share_profile' rel='"+data['gallery_data'][i]['entry_id']+"'></a>";
                                                                     appendBody = appendBody + "</div>";

                                                                 appendBody = appendBody + "</div><br>";
                                                             appendBody = appendBody + "</div>";
                                                         appendBody = appendBody + "</div>";

                                                      appendBody = appendBody + "</div></div></div>";
                                                  var appnedClose = "</li>";

                                                  $("#scroll-it-gallery-sub ul").append(appendOpen + appendBody + appnedClose);
                                            };
                                            


                                        //};
                                    }else{
                                      var msg = "<div class='row text-center'><h5 class='col-xs-12 paddingL-0'>There are no entry at the moment, be the first the join!</h5></div>";
                                      $("#scroll-it-gallery-sub ul").prepend(msg);
                                    }

                              },error: function(e){
                                    //console.log(e);
                              }                                                            
                            });
                        }
                    }

        });

            if ($("#scroll-it-gallery-main").length) {
                   // alert('ss');
                    var gal_count       = $('#gallery_count').val();
                    var loaderContainer = $("#loaderContainerTop");
                    var img             = jQuery("<div style='width:100%; display:block; text-align:center'><img class='imageLoader' src='" + base_url + "assets/images/7152.GIF' style='align:center; margin:0px 0px 10px 0px; height:30px; width:30px;'></div>");                      
                    loaderContainer.html(img);

                   $.ajax({
                      method: "POST",
                      url: base_url + "gallery/fetch_gallery/",
                      data: { from: gal_count, ajax: 1 },
                      success: function(data)
                      {
                            console.log(data);
                            // setTimeout(function() {
                            //   //$('.message').remove();
                            //   img.remove();
                            // }, 2000);

                            $('.imageLoader').hide();

                            if (data['status']) {
                                
                                    // Sums up the total row up this point 
                                    $('#gallery_count').val(Number(data['gallery_data'].length) + Number(gal_count));
                                    for (var i = 0; i < data['gallery_data'].length; i++) {
                                        var name  = data['gallery_data'][i]['name'].split(" ");
                                        var fname = name[0];
                                        var lname = name[1];                                                

                                          var appendOpen = "<li class='col-md-4 col-sm-6 col-xs-6'>";
                                          var appendBody = "<a class='col-md-12 text-center userImage2 effect' target='_blank' href='" + base_url + "gallery/page?u="+data['gallery_data'][i]['entry_id']+"'>";
                                              appendBody = appendBody + "<img src='" + base_url + "entries/" + data['gallery_data'][i]['image_name'] + "' alt=''>";
                                              appendBody = appendBody + "<strong class='col-md-12 userName'>" + fname + "</strong>";
                                              appendBody = appendBody + "</a>";
                                          var appnedClose = "</li>";
                                          $("#scroll-it-gallery-main ul").append(appendOpen + appendBody + appnedClose);                                        
                                    };                                    

                            }else{

                              var msg = "<div class='row text-center'><h5 class='col-xs-12 paddingL-0'><p>There are no entry at the moment, be the first the join!</p></h5></div>";
                              $("#scroll-it-gallery-main ul").prepend(msg);

                            }
                      },error: function(e){
                            console.log(e);
                            $('.imageLoader').hide();

                      }                                                            
                    });                                                

            }            


            if ($("#scroll-it-gallery-sub").length) {
                    var gal_count       = $('#gallery_count').val();
                    var loaderContainer = $("#loaderContainerTopSub");
                    var img             = jQuery("<div style='width:100%; display:block; text-align:center'><img class='imageLoader' src='" + base_url + "assets/images/7152.GIF' style='align:center; margin:0px 0px 10px 0px; height:30px; width:30px;'></div>");                      
                    loaderContainer.html(img);

                   $.ajax({
                      method: "POST",
                      url: base_url + "gallery/fetch_gallery/",
                      data: { from: gal_count, ajax: 1 },
                      success: function(data)
                      {
                            console.log(data);
                            setTimeout(function() {
                              //$('.message').remove();
                              img.remove();
                            }, 2000);

                            if (data['status']) {
                                    // Sums up the total row up this point 
                                    $('#gallery_count').val(Number(data['gallery_data'].length) + Number(gal_count));
                                    for (var i = 0; i < data['gallery_data'].length; i++) {
                                        var name  = data['gallery_data'][i]['name'].split(" ");
                                        var fname = name[0];
                                        var lname = name[1];    

                                          var appendOpen = "<li class='col-xs-4'>";
                                          var appendBody = "<a class='row text-center userImage2 effect' data-toggle='modal' data-target='#" + data['gallery_data'][i]['entry_id'] + "' href='#' onclick='return false'>";
                                              appendBody = appendBody + "<img src='" + base_url + "entries/" + data['gallery_data'][i]['image_name'] + "' alt=''>";
                                              appendBody = appendBody + "<strong class='col-md-12 userName'>" + fname + "</strong>";
                                              appendBody = appendBody + "</a>";
                                              appendBody = appendBody + "<div id='" + data['gallery_data'][i]['entry_id'] + "' class='modal fade' role='dialog' aria-hidden='true'>";
                                              appendBody = appendBody + "<div class='modal-dialog'>";
                                              appendBody = appendBody + "<div class='modal-content col-xs-8 col-xs-offset-2'>";
                                              appendBody = appendBody + "<div class='modal-header row'>";
                                              appendBody = appendBody + "<h5 class='col-xs-8 paddingL-0'>" + data['gallery_data'][i]['name'] + "<h5>";
                                              appendBody = appendBody + "<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>";
                                              appendBody = appendBody + "</div>";
                                              appendBody = appendBody + "<div class='modal-body row'><img src='" + base_url + "entries/" + data['gallery_data'][i]['image_name'] + "' alt='' class=''></div>";
                                              // appendBody = appendBody + "<a href='#/' class='facebook_share_profile' rel='"+data['gallery_data'][i]['name']+"'>facebook</a>";
                                              // appendBody = appendBody + "<a href='#/' class='twitter_share_profile' rel='"+data['gallery_data'][i]['entry_id']+"'>twitter</a>";

                                               appendBody = appendBody + "<div class='row'>";
                                                     appendBody = appendBody + "<div class='col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 text-center share'>";
                                                         appendBody = appendBody + "<div class='row'>";
                                                             appendBody = appendBody + "<div class='col-md-6 col-sm-6 col-xs-6'>";
                                                                 appendBody = appendBody + "<a href='#/' class='facebook-pink pull-right facebook_share_profile' data-eid='" + data['gallery_data'][i]['entry_id'] + "' rel='"+data['gallery_data'][i]['image_name']+"'></a>";
                                                             appendBody = appendBody + "</div>";
                                                             appendBody = appendBody + "<div class='col-md-4 col-sm-4 col-xs-4'>";
                                                                 appendBody = appendBody + "<a href='#/' class='twitter-pink pull-left twitter_share_profile' rel='"+data['gallery_data'][i]['entry_id']+"'></a>";
                                                             appendBody = appendBody + "</div>";

                                                         appendBody = appendBody + "</div><br>";
                                                     appendBody = appendBody + "</div>";
                                                 appendBody = appendBody + "</div>";

                                              appendBody = appendBody + "</div></div></div>";
                                          var appnedClose = "</li>";

                                          $("#scroll-it-gallery-sub ul").append(appendOpen + appendBody + appnedClose);
                                    };
                                    


                                //};
                            }else{
                              var msg = "<div class='row text-center'><h5 class='col-xs-12 paddingL-0'>There are no entry at the moment, be the first the join!</h5></div>";
                              $("#scroll-it-gallery-sub ul").prepend(msg);
                            }
                      },error: function(e){
                            console.log(e);
                      }                                                            
                    });                                                

            }     


           
});

